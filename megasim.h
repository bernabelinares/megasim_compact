/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco
 
 Structure definitions used by the core simulator and the modules
 
*/

#ifndef __MEGASIM_H__
#define __MEGASIM_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/time.h>

#define DEBUG_PRINTS 0
#define WARNING_LOGS 1 

#define COMPS_EV 6 //number of total (components) per event (timing plus non-timing)
#define EVENT_COMPS 3 //number of non-timing components per event (x, y, sign, ...)
#define MAX_EVENTS_IN_BUFFER_CONS 100001 //max events to be stored in temporary consolidated buffer before writing to file
#define MAX_EVENTS_IN_BUFFER_PEND 100001 //pending buffer size
#define MAX_IN_PORTS 100 //max input ports in a module
#define MAX_OUT_PORTS 100 //max output ports in a module
#define MAX_NODE_NAME 60 //max number of chars for node name
#define MAX_NODE_NAMES 400 //max number of chars for list of node names
#define MAX_FILE_NAME 60 //max number of chars for file name
#define SCHEMATICFILE_MAX_LINE_LENGTH 600 //maximum length for input schematic line length
#define PARAMETERFILE_MAX_LINE_LENGTH 600 //maximum length for parameter file line length

extern long conv_events;
extern long conv_connects;

typedef struct
{
	int node_number;                 //node number
	char node_name[MAX_NODE_NAME];   //node name string
	int t_pre_rqst_latest;           //t_pre_rqst most advanced in time for this node
	int t_pre_rqst_earliest_pending; //earliest t_pre_rqst not yet processed
	int prelim_ack;                  //preliminary ack while processing multiple modules connecting to this input node
	int N_prelim_acks;               //Total number of prelim_acks required to consolidate Ack (fanout of this node)
	int n_temp_prelim_acks;          //running number of prelim_acks
	int t_node_busy;                 //time until last processed event is acknowledged
	int n_unprocessed_evs_because_busy; //sometimes an event cannot be processed because the destination module's fifo is full. In this
	                                    //case we need to keep track of this and after processing the full system for a given time_act,
									        //update t_node_busy so that time_act can move forward.
	int flag_unprocessed_evs_because_busy; //flag to signal the above situation is still active
	int prelim_t_node_busy;          //preliminary t_node_busy time for the above situation.
	int nev_cons;                    //number of consolidated events in buffer of consolidated events
	int nev_pending;                 //number of pending events in ring buffer of pending events
	int index_start;                 //start index at ring buffer of pending events
	int index_end;                   //end index at ring buffer of pending events
	int ev_consolidated[MAX_EVENTS_IN_BUFFER_CONS][COMPS_EV]; //buffer of consolidated events
	int ev_pending[MAX_EVENTS_IN_BUFFER_PEND][COMPS_EV];      //ring buffer of pending events
	int is_stimulus;                 //indicates if this node is a stimulus node
	FILE *f_stim;                    //pointer to file with input stimulus list
	char stimulus_file_name[MAX_FILE_NAME]; //name of input stimulus file
	FILE *f_out;                     //pointer to node output file
	char node_ofile_name[MAX_FILE_NAME]; //name of node output file
} node;

typedef struct{
	int* ports;
} ntw;

typedef struct{
	int* prms;
	double* fprms;
	char filename[MAX_FILE_NAME];
} prmss;

int max(int a,int b);
int min(int a,int b);
int write_event(int time_write,int *ev, node *onode, int n_repeat, int delay_to_repeat);
int prelim_handshake(int time_act, int *t_ack, int *ev, node* inode, int *time_module_busy, int delay_to_process, int delay_to_ack, int fifo_depth);
int move_i_events_up(node* nod,int i);
int check_node_times(node* nod);



int init_N_prelim_acks(node* nodes,int total_nodes,int total_modules,ntw** node2modin);
int check_if_ok(int* ok, char* param_name, char* file_name);
int get_node_index(char* nname,char** node_names,int total_nodes);
int handshake(int time_act, node* inode, int final_ack);
int init_allnodes(node *nodes, int n, char **node_names);
int save_and_close_node(node* nod);
int init_stimulus_node(node *nodes,int node_number,char* s);
int find_earliest_pendings_and_busy(int total_nodes,node* nodes,int *list_of_nodes);
int find_earliest_pendings_and_busy(int total_nodes,node* nodes,int *list_of_nodes);
int getintstream(ntw** netw, int index1, int index2, int* out);
int getntw(ntw** netw, int index1, int index2, int* ntwstream, int ntwstream_length);
int first_read_netlist(FILE* fsch, int* total_nodes, int* total_modules, int* T_max,char*** p, char*** pp);
int new_node_name(char *node_name_tmp, char **node_names, int nodes);




#endif
