/*

 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco
 
*/

#include "megasim.h"
#include "module_general.h"


long conv_events = 0;
long conv_connects = 0;

int parser(char** module_lines,char** node_names,node* nodes,int total_nodes,int total_modules,int* list_of_stimu_modules,ntw** node2modin,ntw** modout2node,functionPointer * funPtr,prmss* params,prmss* states,int* n_stim_nodes,int** list_of_stimulus_nodes,char*** stimulus_files_names,functionParamsPointer * funParamsPtr,functionStatesPointer * funStatesPtr);

int main(int argc, char *argv[]) 
{
	clock_t update_start_t, update_end_t;
	FILE *sim_progress;
	struct timeval start_tt, end_tt;
	long secs_used,micros_used;

	clock_t start_t, end_t;
	int sim_counter=0;
	int a, b, j, i, k, total_nodes, total_modules, temp, np, *list_of_nodes, *list_of_stimu_modules, **list_of_stimulus_nodes, n_stim_nodes;
	int time_act, T_max;//ev_in_port[2],ev_out_port[2],state[1];
	int *ntw_stream, *ev_in_port, *ev_out_port, ntwstream_length, repeat, repeat2;
	int prelim_ack[MAX_IN_PORTS],optim_option=0,unixxx=0; //iports2nodes[MAX_IN_PORTS];
	prmss *params, *states;
	ntw **node2modin, **modout2node, *nn;
	node** inodes;//[MAX_IN_PORTS];
	node** onodes;//[MAX_OUT_PORTS];
	node *nodes;
	char ***stimulus_files_names,input_file_name[100],file_termination[10],binfilename[100],**modules_lines,**node_names,**tmpp;
#ifdef __unix__
	struct stat attrib_sch;
	struct stat attrib_bin;
	struct stat attrib_reset = {0};
#endif	
	FILE *fsch, *fconf, *ifconf, *fconf2, *ifconf2;
		
	start_t = clock();
	gettimeofday(&start_tt, NULL);
	// If binary netlist-params-states file is newer than text netlist file, then repeat=1 and we skip parsing the input netlist, 
	// as it has not changed since the previous simulation
	//
	if(argc == 2 || argc == 3){
		strcpy(input_file_name,argv[1]);
		a = strlen(input_file_name);
		memmove(file_termination,&input_file_name[a-4],5); //5 1 more for the '\0'

		if(strcmp(file_termination,".sch") != 0){
			printf("Error: input file name termination must be .sch\n\n");
			exit(1);
		}
		input_file_name[a-4] = '\0';
		sprintf(binfilename,"%s.bin",input_file_name);
		input_file_name[a-4] = '.';
		if((fsch = fopen(input_file_name,"r")) == NULL){
			printf("Error opening file %s : file does not exist\n\n",input_file_name);
			exit(2);
		}
		if(argc == 3){
			strcpy(file_termination,argv[2]);
			if(strcmp(file_termination,"-optim") != 0){
				printf("Error: only option -optim is allowed for now and as 2nd argument.\n\n");
				exit(3);
			}
			optim_option = 1;
			unixxx = 0;
#ifdef __unix__
           unixxx = 1;
			attrib_sch = attrib_reset;
			attrib_bin = attrib_reset;
			stat(input_file_name, &attrib_sch);
			stat(binfilename, &attrib_bin);
			if((long)attrib_sch.st_mtim.tv_sec > (long)attrib_bin.st_mtim.tv_sec)
				repeat = 0;
			if((long)attrib_sch.st_mtim.tv_sec < (long)attrib_bin.st_mtim.tv_sec)
				repeat = 1;
			if((long)attrib_sch.st_mtim.tv_sec == (long)attrib_bin.st_mtim.tv_sec){
				if((long)attrib_sch.st_mtim.tv_nsec > (long)attrib_bin.st_mtim.tv_nsec)
					repeat = 0;
				else
					repeat = 1;
			}
#endif	
           if(unixxx == 0){
			   printf("Error: option -optim can only be used in unix system for now\n\n");
			   exit(0);
		   }
		}
		else
			repeat = 0;
	}
	else{
		printf("Error: Please provide one input netlist file name.\n\n");
		exit(4);
	}
	
	if(repeat == 1){ // if bin files newer than sch file, read initial simulation params 
					  //(total_nodes, total_modules, T_max (total simulation time)) from bin file
		if((ifconf = fopen(binfilename,"rb")) == NULL){
			printf("Error opening file %s\n\n",binfilename);
			exit(5);
		}
		fread(&total_nodes, sizeof(int), 1, ifconf);
		fread(&total_modules, sizeof(int), 1, ifconf);
		fread(&T_max, sizeof(int), 1, ifconf);
		//recover node names
		node_names = malloc(total_nodes * sizeof(char *));
		for(i=0;i<total_nodes;i++){
			fread(&j, sizeof(int), 1, ifconf);
			node_names[i] = malloc(j);
			fread(node_names[i], sizeof(char), j-1, ifconf);
		}
	}
	else{
		//if sch file is newer, then get lines from input netlist for parser, all node names, and
		//initial simulation params (total_nodes, total_modules, T_max (total simulation time))
		first_read_netlist(fsch,&total_nodes,&total_modules,&T_max,&modules_lines,&node_names);
	}
	
	//Initialization of data structures
	functionPointer * funPtr = calloc(total_modules,sizeof(functionPointer)); //each module has its corresponding funtion pointer
	functionParamsPointer * funParamsPtr = calloc(total_modules,sizeof(functionParamsPointer)); //each module has its corresponding params extraction funtion pointer
	functionStatesPointer * funStatesPtr = calloc(total_modules,sizeof(functionStatesPointer)); //each module has its corresponding initial state extraction funtion pointer

    if (funPtr==NULL){
        printf("Main.c: funPtr calloc failed!\n");
        exit(99);
    }
    if (funParamsPtr==NULL){
        printf("Main.c: funParamsPtr calloc failed!\n");
        exit(99);
    }
    if (funStatesPtr==NULL){
        printf("Main.c: funStatesPtr calloc failed!\n");
        exit(99);
    }
	
	list_of_stimu_modules = (int*) calloc(total_modules,sizeof(int));//vector containing (after init) list of modules which are stimulus modules
    if (list_of_stimu_modules==NULL){
        printf("Main.c: list_of_stimu_modules calloc failed!\n");
        exit(99);
    }
    for(i=0;i<total_modules;i++)											// if 0 -> not simulus, if 1 -> module is stimulus
		list_of_stimu_modules[i] = 0;
		
	//list_of_stimulus_nodes = (int*) calloc(1,sizeof(int));
	list_of_stimulus_nodes = (int**) calloc(1,sizeof(int *));
	stimulus_files_names = (char***)malloc(sizeof(char **));
	*stimulus_files_names = (char**)malloc(sizeof(char *));

    
    if (stimulus_files_names==NULL){
        printf("Main.c: stimulus_files_names calloc failed!\n");
        exit(99);
    }

    
    if (list_of_stimulus_nodes==NULL){
        printf("Main.c: list_of_stimulus_nodes calloc failed!\n");
        exit(99);
    }
   
    
	ev_in_port = (int*) calloc(MAX_IN_PORTS,sizeof(int));
	ev_in_port[0] = -1;
    if (ev_in_port==NULL){
        printf("Main.c: ev_in_port calloc failed!\n");
        exit(99);
    }
	
	ev_out_port = (int*) calloc(MAX_OUT_PORTS,sizeof(int));
	ev_out_port[0] = -1;
    if (ev_out_port==NULL){
        printf("Main.c: ev_out_port calloc failed!\n");
        exit(99);
    }
 
	
	list_of_nodes = (int*) calloc(total_nodes+1,sizeof(int)); //this vector holds the node numbers of those nodes with the present 
																    //smallest t_pre_rqst_earliest_pending value. The list finishes with -1.
    
    if (list_of_nodes==NULL){
        printf("Main.c: list_of_nodes calloc failed!\n");
        exit(99);
    }

    
	params = (prmss*) calloc(total_modules,sizeof(prmss)); //params is a one-dimensional vector of pointers to struct prmss.
																  //prmss contains a pointer to integer 1-D array
																  //params[i].prms is a pointer to the 1-D array of int precision parameters of module i.
																  //params[i].fprms is a pointer to the 1-D array of double precision parameters of module i.

    if (params==NULL){
        printf("Main.c: params calloc failed!\n");
        exit(99);
    }

    
    states = (prmss*) calloc(total_modules,sizeof(prmss)); //states is one-dimensional vector of pointers to struct prmss.
																  //prmss contains a pointer to integer 1-D array
																  //states[i].prms is a pointer to the 1-D array of int precision parameters of module i.
																  //states[i].fprms is a pointer to the 1-D array of double precision parameters of module i.
    if (states==NULL){
        printf("Main.c: states calloc failed!\n");
        exit(99);
    }

    
	nodes = (node*) calloc(total_nodes,sizeof(node));
    if (nodes==NULL){
        printf("Main.c: nodes calloc failed!\n");
        exit(99);
    }

	inodes = (node**) calloc(MAX_IN_PORTS,sizeof(node*));
    if (inodes==NULL){
        printf("Main.c: inodes calloc failed!\n");
        exit(99);
    }
    
	onodes = (node**) calloc(MAX_OUT_PORTS,sizeof(node*));
    if (onodes==NULL){
        printf("Main.c: onodes calloc failed!\n");
        exit(99);
    }
	
	
    
    node2modin = (ntw**) calloc(total_nodes,sizeof(ntw));      //node2modin is a 2-D array of struct ntw, defining connections from nodes to modules.
    if (node2modin==NULL){
        printf("Main.c: node2modin calloc failed!\n");
        exit(99);
    }
    nn = (ntw*) calloc(total_nodes*total_modules,sizeof(ntw)); //node2modin[node][module].ports is a 1-D array of integers indicating the
    if (nn==NULL){
        printf("Main.c: node2modin nn calloc failed!\n");
        exit(99);
    }
    for(i=0;i<total_nodes;i++)                                 //module's "module" input ports connecting to node "node".
		node2modin[i] = &nn[i*total_modules]; 
	
    
    
	modout2node = (ntw**) calloc(total_modules,sizeof(ntw));   //modout2node is a 2-D array of struct ntw, defining connections from modules to nodes.
    if (modout2node==NULL){
        printf("Main.c: modout2node calloc failed!\n");
        exit(99);
    }
    nn = (ntw*) calloc(total_nodes*total_modules,sizeof(ntw)); //modout2node[module][node].ports is a 1-D array of integers indicating the
    if (nn==NULL){
        printf("Main.c: modout2node nn calloc failed!\n");
        exit(99);
    }
    for(i=0;i<total_modules;i++)                               //module's "module" output ports connecting to node "node".
		modout2node[i] = &nn[i*total_nodes];
		
	init_allnodes(nodes,total_nodes,node_names);
	
	if(repeat == 0){
		//call parser here using lines from input netlist
		parser(modules_lines,node_names,nodes,total_nodes,total_modules,list_of_stimu_modules,node2modin,modout2node,funPtr,params,states,&n_stim_nodes,list_of_stimulus_nodes,stimulus_files_names,funParamsPtr,funStatesPtr);
 		//init_network_test_example(nodes,total_nodes,total_modules,list_of_stimu_modules,node2modin,modout2node,funPtr,params,states,&n_stim_nodes,list_of_stimulus_nodes,stimulus_files_names);

		//If first time running the netlist (repeat=0) and -optim set, save all data necessary for next run
		if(optim_option == 1){
			fconf = fopen(binfilename,"wb");
			fwrite(&total_nodes, sizeof(int), 1, fconf);
			fwrite(&total_modules, sizeof(int), 1, fconf);
			fwrite(&T_max, sizeof(int), 1, fconf);
			//save node names
			for(i=0;i<total_nodes;i++){
				j = strlen(node_names[i])+1;
				fwrite(&j, sizeof(int), 1, fconf);
				fwrite(node_names[i], strlen(node_names[i]), 1, fconf);
			}
		
			//save list of stimulus modules
			fwrite(list_of_stimu_modules, sizeof(int), total_modules, fconf);
		
			//convert ntw node2modin to int stream and save
			ntw_stream = (int*) calloc(2*total_modules*total_nodes,sizeof(int));
			ntwstream_length = getintstream(node2modin,total_nodes,total_modules,ntw_stream);
			fwrite(&ntwstream_length, sizeof(int),1,fconf);
			fwrite(ntw_stream, sizeof(int), ntwstream_length, fconf);
		
			//convert ntw modout2node to int stream and save
			ntw_stream = (int*) calloc(2*total_modules*total_nodes,sizeof(int));
			ntwstream_length = getintstream(modout2node,total_modules,total_nodes,ntw_stream);
			fwrite(&ntwstream_length, sizeof(int),1,fconf);
			fwrite(ntw_stream, sizeof(int), ntwstream_length, fconf);
		
			//save function pointers
			fwrite(funPtr, sizeof(functionPointer), total_modules, fconf); 
			fwrite(funParamsPtr, sizeof(functionParamsPointer), total_modules, fconf);
			fwrite(funStatesPtr, sizeof(functionParamsPointer), total_modules, fconf);
		
			//save stimulus files number, nodes, and names
			fwrite(&n_stim_nodes, sizeof(int), 1, fconf);
			fwrite(*list_of_stimulus_nodes, sizeof(int), n_stim_nodes, fconf);
			for(i=0;i<n_stim_nodes;i++){
				j = strlen((*stimulus_files_names)[i])+1;
				fwrite(&j, sizeof(int), 1, fconf);
				fwrite((*stimulus_files_names)[i], j, 1, fconf);
			}
		
			//save params and states
			for(i=0;i<total_modules;i++)
				if(list_of_stimu_modules[i] == 0)
					{
						// save params files names
						j = strlen(params[i].filename);
						fwrite(&j, sizeof(int), 1, fconf);
						fwrite(params[i].filename, sizeof(char), j, fconf);
					
						// save params in separate bin files
						strcpy(binfilename,params[i].filename);
						strcat(binfilename,"bin");
						fconf2 = fopen(binfilename,"wb");
						fwrite(params[i].prms, sizeof(int), params[i].prms[0]+2, fconf2);
						if(params[i].prms[1] != 0)
							fwrite(params[i].fprms, sizeof(double), params[i].prms[1], fconf2);
						fclose(fconf2);
					
						// save states files names
						j = strlen(states[i].filename);
						fwrite(&j, sizeof(int), 1, fconf);
						fwrite(states[i].filename, sizeof(char), j, fconf);
					
						// save states on separate bin files
						strcpy(binfilename,states[i].filename);
						strcat(binfilename,"bin");
						fconf2 = fopen(binfilename,"wb");
						fwrite(states[i].prms, sizeof(int), states[i].prms[0]+2, fconf2);
						if(states[i].prms[1] != 0)
							fwrite(states[i].fprms, sizeof(double), states[i].prms[1], fconf2);
						fclose(fconf2);
					} 
		
			fclose(fconf);
		}
	}
	else{
	
		fread(list_of_stimu_modules, sizeof(int), total_modules, ifconf); 
		
		// recover ntw node2modin from int stream
		fread(&ntwstream_length, sizeof(int),1,ifconf);
		ntw_stream = (int*) calloc(ntwstream_length,sizeof(int));
		fread(ntw_stream, sizeof(int), ntwstream_length, ifconf);
		getntw(node2modin,total_nodes,total_modules,ntw_stream,ntwstream_length);
		free(ntw_stream);
		
		// recover ntw modout2node from int stream
		fread(&ntwstream_length, sizeof(int),1,ifconf);
		ntw_stream = (int*) calloc(ntwstream_length,sizeof(int));
		fread(ntw_stream, sizeof(int), ntwstream_length, ifconf);
		getntw(modout2node,total_modules,total_nodes,ntw_stream,ntwstream_length);
		free(ntw_stream);
		
		// recover function pointers
		fread(funPtr, sizeof(functionPointer), total_modules, ifconf);
		fread(funParamsPtr, sizeof(functionParamsPointer), total_modules, ifconf);
		fread(funStatesPtr, sizeof(functionParamsPointer), total_modules, ifconf);
		
		// recover stimulus files number, nodes, and names
		fread(&n_stim_nodes, sizeof(int), 1, ifconf);
		*list_of_stimulus_nodes = (int*) calloc(n_stim_nodes, sizeof(int));
		fread(*list_of_stimulus_nodes, sizeof(int), n_stim_nodes, ifconf);
		tmpp = (char**) realloc(*stimulus_files_names, n_stim_nodes * sizeof(char *));
		if(tmpp == NULL){
			printf("Error reallocating memory for *stimulus_files_names in main\n\n");
			exit(6);
		}
		*stimulus_files_names = tmpp;
		for(i=0;i<n_stim_nodes;i++){
			fread(&j, sizeof(int), 1, ifconf);
			(*stimulus_files_names)[i] = malloc(j);
			fread((*stimulus_files_names)[i], sizeof(char), j, ifconf);
		}
		 
		// recover params and states
		for(i=0;i<total_modules;i++)
			if(list_of_stimu_modules[i] == 0)
				{
					// recover params files names
					fread(&j, sizeof(int), 1, ifconf);
					fread(params[i].filename, sizeof(char), j, ifconf);
					
					// recover params from separate bin files
					strcpy(binfilename,params[i].filename);
					strcat(binfilename,"bin");					
					// first we check if text param file is newer than bin param file
					repeat2 = 0;
#ifdef __unix__
					attrib_sch = attrib_reset;
					attrib_bin = attrib_reset;
					stat(params[i].filename, &attrib_sch);
					stat(binfilename, &attrib_bin);
					if((long)attrib_sch.st_mtim.tv_sec > (long)attrib_bin.st_mtim.tv_sec)
						repeat2 = 0; // ascii file is newer
					if((long)attrib_sch.st_mtim.tv_sec < (long)attrib_bin.st_mtim.tv_sec)
						repeat2 = 1; // bin file is newer
					if((long)attrib_sch.st_mtim.tv_sec == (long)attrib_bin.st_mtim.tv_sec){
						if((long)attrib_sch.st_mtim.tv_nsec > (long)attrib_bin.st_mtim.tv_nsec)
							repeat2 = 0; // ascii file is newer
						else
							repeat2 = 1; // bin file is newer
					}
#endif
					if(repeat2 == 0){
						funParamsPtr[i](&(params[i]),params[i].filename); //same in parser()
						// save params in separate bin files
						strcpy(binfilename,params[i].filename);
						strcat(binfilename,"bin");
						fconf2 = fopen(binfilename,"wb");
						fwrite(params[i].prms, sizeof(int), params[i].prms[0]+2, fconf2);
						if(params[i].prms[1] != 0)
							fwrite(params[i].fprms, sizeof(double), params[i].prms[1], fconf2);
						fclose(fconf2);
						}
					else{
						if((ifconf2 = fopen(binfilename,"rb")) == NULL){
							printf("Error opening file %s\n\n",binfilename);
							exit(7);
						}
						params[i].prms = (int*) calloc(2,sizeof(int));
						fread(&(params[i].prms[0]),sizeof(int),1, ifconf2);
						fread(&(params[i].prms[1]),sizeof(int),1, ifconf2);
						if(params[i].prms[0] != 0){
							params[i].prms = realloc(params[i].prms, (params[i].prms[0]+2)*sizeof(int));
							fread(&(params[i].prms[2]), sizeof(int), params[i].prms[0], ifconf2);
						}
						if(params[i].prms[1] != 0){
							params[i].fprms = (double*) calloc(params[i].prms[1], sizeof(double));
							fread(params[i].fprms, sizeof(double), params[i].prms[1], ifconf2);
						}
						fclose(ifconf2);
					}

					// recover states files names
					fread(&j, sizeof(int), 1, ifconf);
					fread(states[i].filename, sizeof(char), j, ifconf);

					// recover states from separate bin files
					strcpy(binfilename,states[i].filename);
					strcat(binfilename,"bin");
					// first we check if text state file is newer than bin state file
					repeat2 = 0;
#ifdef __unix__
					attrib_sch = attrib_reset;
					attrib_bin = attrib_reset;
					stat(states[i].filename, &attrib_sch);
					stat(binfilename, &attrib_bin);
					if((long)attrib_sch.st_mtim.tv_sec > (long)attrib_bin.st_mtim.tv_sec)
						repeat2 = 0; // ascii file is newer
					if((long)attrib_sch.st_mtim.tv_sec < (long)attrib_bin.st_mtim.tv_sec)
						repeat2 = 1; // bin file is newer
					if((long)attrib_sch.st_mtim.tv_sec == (long)attrib_bin.st_mtim.tv_sec){
						if((long)attrib_sch.st_mtim.tv_nsec > (long)attrib_bin.st_mtim.tv_nsec)
							repeat2 = 0; // ascii file is newer
						else
							repeat2 = 1; // bin file is newer
					}
#endif
					if(repeat2 == 0){
						funStatesPtr[i](&(states[i]),states[i].filename,&(params[i])); //same in parser()
						// save params in separate bin files
						strcpy(binfilename,states[i].filename);
						strcat(binfilename,"bin");
						fconf2 = fopen(binfilename,"wb");
						fwrite(states[i].prms, sizeof(int), states[i].prms[0]+2, fconf2);
						if(states[i].prms[1] != 0)
							fwrite(states[i].fprms, sizeof(double), states[i].prms[1], fconf2);
						fclose(fconf2);
						}
					else{
						if((ifconf2 = fopen(binfilename,"rb")) == NULL){
							printf("Error opening file %s\n\n",binfilename);
							exit(8);
						}
						states[i].prms = (int*) calloc(2,sizeof(int));
						fread(&(states[i].prms[0]),sizeof(int),1, ifconf2);
						fread(&(states[i].prms[1]),sizeof(int),1, ifconf2);
						if(states[i].prms[0] != 0){
							states[i].prms = realloc(states[i].prms, (states[i].prms[0]+2)*sizeof(int));
							fread(&(states[i].prms[2]), sizeof(int), states[i].prms[0], ifconf2);
						}
						if(states[i].prms[1] != 0){
							states[i].fprms = (double*) calloc(states[i].prms[1], sizeof(double));
							fread(states[i].fprms, sizeof(double), states[i].prms[1], ifconf2);
						}
						fclose(ifconf2);
					}
				}
		
		// initialize stimulus nodes
		for(i=0;i< n_stim_nodes;i++)
			init_stimulus_node(nodes,i,(*stimulus_files_names)[i]);
		
		fclose(ifconf);
	}
	
	init_N_prelim_acks(nodes,total_nodes,total_modules,node2modin);
	
	update_start_t=clock();
	printf("Start Processing Events\n");
   
    
   time_act = 0;
   temp = find_earliest_pendings_and_busy(total_nodes,nodes,list_of_nodes);//list_of_nodes contains all nodes with pending event at earliest time
   
   while((list_of_nodes[0] != -1) && (temp < T_max)){
		if(temp < time_act){
			printf("Error in main: temp(%d)<time_act(%d)\n\n",temp,time_act);exit(9);}
		time_act = temp;
       
		for(i=0;i<total_modules;i++)
		  if(list_of_stimu_modules[i] == 0){ //i is module index. If 0 it is not stimulus node, if 1 it is stimulus node.
			b=params[i].prms[0+2]; //b contains number of input ports of module i
			
			//build ev_in_port[]
			for(j=0;j<b;j++){
				ev_in_port[j] = 0; // initialize vector ev_in_port to all 0
				prelim_ack[j] = -1; //initialize prelim_ack
			}
			ev_in_port[b] = -1;
			prelim_ack[b] = -1;
			a=0;
			while((np=list_of_nodes[a]) != -1){ //np is node index
				a++;
				if(node2modin[np][i].ports[0] != -1){
					j=0;
					while((k=node2modin[np][i].ports[j]) != -1){ //ev_in_port contains the list of input ports of module i connected to node np
						 		                      			   // with a pending event at the present time
						if(nodes[np].flag_unprocessed_evs_because_busy == 1){ //this node requires pending prelim_acks
							if((states[i].prms)[2+1+k] == -1)  //this input port has already contributed prelim_acks
								ev_in_port[k]=0;
							else                             //this input port is pending a prelim_ack
								ev_in_port[k]=1;
						}
						else
							ev_in_port[k]=1;
						j++;
					}
				}
			}
				
			//build inodes for module i
			for(np=0;np<total_nodes;np++){
				j=0;
				while((k=node2modin[np][i].ports[j]) != -1){
					inodes[k] = &(nodes[np]);
					j++;
				}
			}
              //printf("Module %d has %d input nodes\n",i,j);
			
			//build onodes for module i
			for(np=0;np<total_nodes;np++){
				j=0;
				while((k=modout2node[i][np].ports[j]) != -1){
					onodes[k] = &nodes[np];
					j++;
				}
			}
			//printf("Module %d has %d output nodes\n",i,j);
              
			//call module i
//			(*(funPtr[i]))(time_act,prelim_ack,ev_in_port,inodes,onodes,params[i].prms,states[i].prms,params[i].fprms,states[i].fprms);
              //printf("module %d is %s total nodes %d\n",i,nodes[i].node_ofile_name,total_nodes);
			(*(funPtr[i]))(time_act,prelim_ack,ev_in_port,inodes,onodes,&params[i],&states[i]);
		}
		a=0;
		while((np=list_of_nodes[a]) != -1){ //np is node index
		    a++;
			if(nodes[np].N_prelim_acks == nodes[np].n_temp_prelim_acks){ //event has been acked by all destination modules
				nodes[np].t_node_busy = nodes[np].prelim_ack;
				nodes[np].prelim_ack = -1;
				nodes[np].n_temp_prelim_acks = 0;
				handshake(time_act,&(nodes[np]),nodes[np].t_node_busy);
				if(nodes[np].flag_unprocessed_evs_because_busy == 1)
					nodes[np].flag_unprocessed_evs_because_busy = 0;
			}
			else if(nodes[np].N_prelim_acks == nodes[np].n_temp_prelim_acks + nodes[np].n_unprocessed_evs_because_busy){ //event has NOT been acked by all destination modules, because some where busy with full fifo. Need to be completed in a future time.
				if(nodes[np].prelim_ack != -1)
					nodes[np].t_node_busy = max(nodes[np].prelim_ack,nodes[np].prelim_t_node_busy);
				else
					nodes[np].t_node_busy = nodes[np].prelim_t_node_busy;
				nodes[np].n_unprocessed_evs_because_busy = 0;
				nodes[np].flag_unprocessed_evs_because_busy = 1;
			}
			else{
				printf("Error in main: something went wrong with prelim_acks and n_temp_prelim_acks\n\n");
				printf("We will ignore you for the time being\n");
				exit(10);
			}
		}
		temp = find_earliest_pendings_and_busy(total_nodes,nodes,list_of_nodes);//list_of_nodes contains all nodes with pending event at earliest time

		//added this for the interactive plot
		sim_counter++;
		if (!(sim_counter%10000))
		{
			update_end_t=clock();
			sim_progress = fopen("megasim_progress.tmp","w");
			fprintf(sim_progress,"# current event time, real time since the beginning of the simulation in seconds, update time in seconds\n");
			fprintf(sim_progress,"%d %f %f\n",time_act,(float)(clock()-start_t)/CLOCKS_PER_SEC ,(float)(update_end_t-update_start_t)/CLOCKS_PER_SEC);
			//printf("Current event time is: %d Real simulation time is: %f s Update time is: %f s \n",time_act,(float)(clock()-start_t)/CLOCKS_PER_SEC ,(float)(update_end_t-update_start_t)/CLOCKS_PER_SEC);
            printf("Events processed: %.2f %%, Current event TS: %d, Update time is: %f s \n",((float)time_act/T_max)*100.0,time_act,(float)(update_end_t-update_start_t)/CLOCKS_PER_SEC);
            
            fclose(sim_progress);
			update_start_t=clock();
		}  //bern

   }
   
   for(i=0;i<total_nodes;i++)
	   save_and_close_node(&nodes[i]);

	free(funPtr);
	free(list_of_stimu_modules);
	free(ev_in_port);
	free(ev_out_port);
	free(list_of_nodes);
	for(i=0;i<total_modules;i++){
		free(params[i].prms);
		free(params[i].fprms);
		free(states[i].prms);
		free(states[i].fprms);
	}
	free(params);
	free(states);
	free(nodes);
	free(inodes);	
	free(onodes);
	for(i=0;i<total_nodes;i++)
		for(j=0;j<total_modules;j++){
			free(node2modin[i][j].ports);
			free(modout2node[j][i].ports);
		}
	free(node2modin);
	free(modout2node);
	free(nn);

	end_t = clock();
	gettimeofday(&end_tt, NULL);
	double tot_time;
	//printf("end_t = %f\n",(double)end_t);
	//printf("start_t = %f\n",(double)start_t);
	//printf("CLOCKS_PER_SEC = %ld\n",CLOCKS_PER_SEC);
	tot_time = (double)(end_t - start_t) / (double)CLOCKS_PER_SEC;
	secs_used=(end_tt.tv_sec - start_tt.tv_sec); //avoid overflow by subtracting first
    micros_used= ((secs_used*1000000) + end_tt.tv_usec) - (start_tt.tv_usec);
	
	printf("End of program.\nTotal CPU time: %f (sec)\n", tot_time);
	printf("Total elapsed time: %f (sec)\n",((double) micros_used)/1e6);
	printf("Total conv_module events processed: %e (%e events per CPU sec)\n", (double) conv_events,((double) conv_events)/tot_time);
	printf("Total conv_module connections: %e (%e connections per CPU sec)\n\n", (double) conv_connects,((double) conv_connects)/tot_time);
	
   return 0;
}


int parser(char** module_lines,char** node_names,node* nodes,int total_nodes,int total_modules,int* list_of_stimu_modules,ntw** node2modin,ntw** modout2node,functionPointer * funPtr,prmss* params,prmss* states,int* n_stim_nodes,int** list_of_stimulus_nodes,char*** stimulus_files_names,functionParamsPointer * funParamsPtr,functionStatesPointer * funStatesPtr)
{
    int i,j,jj,k,l,nnode,nport,last,*temp,stimNodes=0,size=100;
    char c,line[SCHEMATICFILE_MAX_LINE_LENGTH], sect[SCHEMATICFILE_MAX_LINE_LENGTH], nname[MAX_NODE_NAME], **temp2;
    
    for(i=0;i<total_nodes;i++)
        for(j=0;j<total_modules;j++){
            node2modin[i][j].ports = (int*) calloc(1,sizeof(int));
            node2modin[i][j].ports[0] = -1;//
            modout2node[j][i].ports = (int*) calloc(1,sizeof(int));
            modout2node[j][i].ports[0] = -1;//
        }
    
    *list_of_stimulus_nodes = (int*) calloc(size,sizeof(int));
    
    for(i=0;i<total_modules;i++){
        strcpy(line,module_lines[i]);
        j = 0;
        while((c = line[j++]) != ' ');
        line[j-1] = '\0';
        
        // is module source/stimulus?
        if(strcmp(line,"source") == 0){
            while((c = line[j++]) != '{');
            k=0;
            while((c = line[j++]) != '}')
                sect[k++] = c;
            sect[k] = '\0';
            if(k >= MAX_FILE_NAME - 1){
                printf("File name %s must be shorter than MAX_FILE_NAME (%d)\n\n",sect,MAX_FILE_NAME);
                exit(48);
            }
            nnode=0;
            nport=0;
            jj=0;
            k=0;
            last=0;
            while(last == 0){
                while(((c = sect[k++]) != '\0') && c != ',')
                    nname[jj++] = c;
                if(c == '\0')
                    last = 1;
                if(last == 0){
                    printf("Error in input line of source: %s. Only one ouput node is allowed\n\n",line);
                    exit(49);
                }
                nname[jj] = '\0';
                if(jj >= MAX_NODE_NAME - 1){
                    printf("Node name %s must be shorter than MAX_NODE_NAME (%d)\n\n",nname,MAX_NODE_NAME);
                    exit(50);
                }
                nnode = get_node_index(nname,node_names,total_nodes);
                
                // update list_of_stimulus_nodes
                if(stimNodes == size){
                    size = size*2; // why *2?
                    temp = (int*) realloc(*list_of_stimulus_nodes,size*sizeof(int));
                    if(temp == NULL){
                        printf("Error with realloc of list_of_stimulus_nodes in parser\n\n");
                        exit(51);
                    }
                    *list_of_stimulus_nodes = temp;
                }
                (*list_of_stimulus_nodes)[stimNodes] = nnode;
                stimNodes++;
                
                //update list_of_stimu_modules
                list_of_stimu_modules[i] = 1;
                
                // update modout2node of stimulus node
                l=0;
                while(modout2node[i][nnode].ports[l] != -1) l++;
                l++;
                modout2node[i][nnode].ports[l-1] = nport++;
                temp = realloc(modout2node[i][nnode].ports, (l+1) * sizeof(int));
                if(temp == NULL){
                    printf("Error with realloc\n\n");exit(52);
                }
                modout2node[i][nnode].ports = temp;
                modout2node[i][nnode].ports[l] = -1;
            }
            //adjust final size of list_of_stimulus_nodes
            temp = (int*) realloc(*list_of_stimulus_nodes,stimNodes*sizeof(int));
            if(temp == NULL){
                printf("Error adjusting final size for list_of_stimulus_nodes in parser\n\n");
                exit(53);
            }
            *list_of_stimulus_nodes = temp;
            
            //handle stimulus file
            while((c = line[j]) == ' ') j++;
            k=0;
            while(((c = line[j++]) != ' ') && (c != '\0'))
                sect[k++] = c;
            sect[k] = '\0';
            if(k >= MAX_FILE_NAME - 1){
                printf("File name %s must be shorter than MAX_FILE_NAME (%d)\n\n",sect,MAX_FILE_NAME);
                exit(54);
            }
            temp2 = (char**) realloc(*stimulus_files_names, (stimNodes) * sizeof(char *));
            if(temp2 == NULL){
                printf("Error reallocating memory for stimulus_files_names in parser\n\n");
                exit(55);
            }
            *stimulus_files_names = temp2;
            (*stimulus_files_names)[stimNodes-1] = malloc(strlen(sect)+1);
            strcpy((*stimulus_files_names)[stimNodes-1],sect);
        }
        // Otherwise, module is not source
        else{
            // get module name and set module function
            for(k=0;k<totalFuncs;k++)
                if(strcmp(line,FunctionsCatalog[k]) == 0){
                    funPtr[i] = funPtrCatalog[k];
                    funParamsPtr[i] = funPtrCatalogParams[k];
                    funStatesPtr[i] = funPtrCatalogStates[k];
                }
            
            // Do now for input nodes of module
            while((c = line[j++]) != '{');
            k=0;
            while((c = line[j++]) != '}')
                sect[k++] = c;
            sect[k] = '\0';
            if(k >= MAX_NODE_NAMES - 1){
                printf("Node name %s must be shorter than MAX_NODE_NAME (%d)\n\n",sect,MAX_NODE_NAMES);
                exit(56);
            }
            nnode=0;
            nport=0;
            k=0;
            last=0;
            while(last == 0){
                jj=0;
                while(((c = sect[k++]) != '\0') && c != ',')
                    nname[jj++] = c;
                if(c == '\0')
                    last = 1;
                nname[jj] = '\0';
                if(jj >= MAX_NODE_NAME - 1){
                    printf("Node name %s must be shorter than MAX_NODE_NAME (%d)\n\n",nname,MAX_NODE_NAME);
                    exit(57);
                }
                nnode = get_node_index(nname,node_names,total_nodes);
                l=0;
                while(node2modin[nnode][i].ports[l++] != -1);
                node2modin[nnode][i].ports[l-1] = nport++;
                temp = realloc(node2modin[nnode][i].ports, (l+1) * sizeof(int));
                if(temp == NULL){
                    printf("Error with realloc\n\n");exit(58);
                }
                node2modin[nnode][i].ports = temp;
                node2modin[nnode][i].ports[l] = -1;
            }
            
            // Do now for output nodes of module
            while((c = line[j++]) != '{');
            k=0;
            while((c = line[j++]) != '}')
                sect[k++] = c;
            sect[k] = '\0';
            nnode=0;
            nport=0;
            k=0;
            last=0;
            while(last == 0){
                jj=0;
                while(((c = sect[k++]) != '\0') && c != ',')
                    nname[jj++] = c;
                if(c == '\0')
                    last = 1;
                nname[jj] = '\0';
                if(jj >= MAX_NODE_NAME - 1){
                    printf("Node name %s must be shorter than MAX_NODE_NAME (%d)\n\n",nname,MAX_NODE_NAME);
                    exit(59);
                }
                nnode = get_node_index(nname,node_names,total_nodes);
                // update modout2node of stimulus node
                l=0;
                while(modout2node[i][nnode].ports[l] != -1) l++;
                l++;
                modout2node[i][nnode].ports[l-1] = nport++;
                temp = realloc(modout2node[i][nnode].ports, (l+1) * sizeof(int));
                if(temp == NULL){
                    printf("Error with realloc\n\n");exit(60);
                }
                modout2node[i][nnode].ports = temp;
                modout2node[i][nnode].ports[l] = -1;
            }
            
            //handle module parameter file
            while((c = line[j]) == ' ') j++;
            k=0;
            while(((c = line[j++]) != ' ') && (c != '\0') && (c != '\n'))
                sect[k++] = c;
            sect[k] = '\0';
            if(k >= MAX_FILE_NAME - 1){
                printf("File name %s must be shorter than MAX_FILE_NAME (%d)\n\n",sect,MAX_FILE_NAME);
                exit(61);
            }
            strcpy(params[i].filename,sect);
            funParamsPtr[i](&(params[i]),sect);
            
            //handle module states file
            while((c = line[j]) == ' ') j++;
            k=0;
            while(((c = line[j++]) != ' ') && (c != '\0') && (c != '\n'))
                sect[k++] = c;
            sect[k] = '\0';
            if(k >= MAX_FILE_NAME - 1){
                printf("File name %s must be shorter than MAX_FILE_NAME (%d)\n\n",sect,MAX_FILE_NAME);
                exit(62);
            }
            strcpy(states[i].filename,sect);
            funStatesPtr[i](&(states[i]),sect,&(params[i]));
        }
        
    }
    
    *n_stim_nodes = stimNodes;
    for(i=0;i< stimNodes;i++)
        init_stimulus_node(nodes,i,(*stimulus_files_names)[i]);
    
    return 0;
}

