/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Evangelos Stromatias, Bernabe Linares-Barranco
 
*/

/*
 * module_stdp.c
 *
 *  Created on: 27 Jan 2016
 *      Event-driven stdp for spiking convolutional networks.
 *      Need 1 stdp module per convolutional layer
 *
 *      TODO:
 *
 */


#include "megasim.h"

#define NKPARAMS 11
#define NOPARAMS 12
#define KKPARAMS 4


int module_stdp(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes,
		node** onodes, prmss *params,prmss *state);
int module_stdp_params(prmss *params, char *prm_file_name);
int module_stdp_states(prmss *states, char *prm_file_name, prmss *params);


int module_stdp(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes, node** onodes, prmss *params,prmss *state)
{
    printf("Please contact the workgroup leader to receive a pre-compiled binary\n");
    exit(999);

return 0;
}


int module_stdp_params(prmss *params, char *prm_file_name)
{
    printf("Please contact the workgroup leader to receive a pre-compiled binary\n");
    exit(999);

	return 0;
}

int module_stdp_states(prmss *states, char *prm_file_name, prmss *params)
{

    printf("Please contact the workgroup leader to receive a pre-compiled binary\n");
    exit(999);

	return 0;

}


