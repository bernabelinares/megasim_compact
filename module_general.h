/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco

 Function pointer definitions required by the modules
 
*/

#ifndef __MODULE_GENERAL_H__
#define __MODULE_GENERAL_H__

#include "megasim.h"

// Functions (Modules) declarations
//typedef int (*functionPointer)(int ,int *,int *,node** ,node** ,int *,int *,double *,double *);
typedef int (*functionPointer)(int ,int *,int *,node** ,node** ,prmss*,prmss*);
	int module_copy(); //declare here all available modules
	int module_conv();
	int module_conv_plastic();
	int module_stdp();
    int module_stdp_orderbased();
	int totalFuncs = 5; //indicate total number of available modules
	char *FunctionsCatalog[] = {"module_copy","module_conv","module_conv_plastic","module_stdp","module_stdp_orderbased"}; //strings with modules names
	functionPointer funPtrCatalog[] = {&module_copy,&module_conv,&module_conv_plastic,&module_stdp,&module_stdp_orderbased}; //catalog of functions, as array of funtion pointers

// Functions for parameter extraction from parameter files
typedef int (*functionParamsPointer)(prmss*, char*);
	int module_copy_params();
	int module_conv_params();
	int module_conv_plastic_params();
	int module_stdp_params();
    int module_stdp_orderbased_params();
	char *FunctionsCatalogParams[] = {"module_copy_params","module_conv_params","module_conv_plastic_params","module_stdp","module_stdp_orderbased_params"}; //strings with parameter extracting functions of modules
	functionParamsPointer funPtrCatalogParams[] = {&module_copy_params,&module_conv_params,&module_conv_plastic_params,&module_stdp_params,&module_stdp_orderbased_params}; //catalog of parameter extraction functions

// Functions for initial state extraction from initial state files
typedef int (*functionStatesPointer)(prmss*, char*, prmss*);
	int module_copy_states();
	int module_conv_states();
	int module_conv_plastic_states();
	int module_stdp_states();
    int module_stdp_orderbased_states();
	char *FunctionsCatalogStates[] = {"module_copy_states","module_conv_states","module_conv_plastic_states","module_stdp_states","module_stdp_orderbased_states"}; //strings with initial state extracting functions of modules
	functionStatesPointer funPtrCatalogStates[] = {&module_copy_states,&module_conv_states,&module_conv_plastic_states,&module_stdp_states,&module_stdp_orderbased_states}; //catalog of state extraction functions

#endif
