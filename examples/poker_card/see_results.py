import numpy as np
import pylab as plt

#A list that holds the events of the output population
outputFiles = ["node_l4_1.evs","node_l4_2.evs","node_l4_3.evs","node_l4_4.evs"]
# A list that holds the marker type for plotting
plotSymbols = ["o","*","+","^"]
# A list that holds the colors for plotting
plotColors = ["b","r","g","k"]


for i,datafile in enumerate(outputFiles):
    # For each event file, open it and convert it to integers
    open_events_file = np.genfromtxt(datafile,delimiter=" ",dtype="int")
    # Return the indeces where events are ON
    indeces_of_ON_events = np.where(open_events_file[:,5]==1)[0]
    # Scatter plot
    plt.scatter(open_events_file[indeces_of_ON_events][:,0]/1e9,(i+1)*np.ones(len((open_events_file[indeces_of_ON_events][:,0]))), marker=plotSymbols[i],color=plotColors[i])

# Show the plot, matplotlib doesn't require a hold on 
plt.show()
