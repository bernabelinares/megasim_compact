.netlist
source {input} cardsin.stim
module_conv {input} {l1_1} gabor_001_orig.prm gab_001.stt
module_conv {input} {l1_2} gabor_002_orig.prm gab_002.stt
module_conv {input} {l1_3} gabor_003_orig.prm gab_003.stt
module_conv {input} {l1_4} gabor_004_orig.prm gab_004.stt
module_conv {input} {l1_5} gabor_005_orig.prm gab_005.stt
module_conv {input} {l1_6} gabor_006_orig.prm gab_006.stt
module_conv {l1_1,l1_2,l1_3,l1_4,l1_5,l1_6} {l2_1} gabor_011_orig.prm gab_011.stt
module_conv {l1_1,l1_2,l1_3,l1_4,l1_5,l1_6} {l2_2} gabor_012_orig.prm gab_012.stt
module_conv {l1_1,l1_2,l1_3,l1_4,l1_5,l1_6} {l2_3} gabor_013_orig.prm gab_013.stt
module_conv {l1_1,l1_2,l1_3,l1_4,l1_5,l1_6} {l2_4} gabor_014_orig.prm gab_014.stt
module_conv {l2_1} {l2_1s} gabor_011s.prm gab_011s.stt
module_conv {l2_2} {l2_2s} gabor_012s.prm gab_012s.stt
module_conv {l2_3} {l2_3s} gabor_013s.prm gab_013s.stt
module_conv {l2_4} {l2_4s} gabor_014s.prm gab_014s.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_1} gabor_021.prm gab_021.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_2} gabor_022.prm gab_022.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_3} gabor_023.prm gab_023.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_4} gabor_024.prm gab_024.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_5} gabor_025.prm gab_025.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_6} gabor_026.prm gab_026.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_7} gabor_027.prm gab_027.stt
module_conv {l2_1s,l2_2s,l2_3s,l2_4s} {l3_8} gabor_028.prm gab_028.stt
module_conv {l3_1,l3_2,l3_3,l3_4,l3_5,l3_6,l3_7,l3_8} {l4_1} gabor_031.prm gab_031.stt
module_conv {l3_1,l3_2,l3_3,l3_4,l3_5,l3_6,l3_7,l3_8} {l4_2} gabor_032.prm gab_032.stt
module_conv {l3_1,l3_2,l3_3,l3_4,l3_5,l3_6,l3_7,l3_8} {l4_3} gabor_033.prm gab_033.stt
module_conv {l3_1,l3_2,l3_3,l3_4,l3_5,l3_6,l3_7,l3_8} {l4_4} gabor_034.prm gab_034.stt

.options
Tmax = 429290101
