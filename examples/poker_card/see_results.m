load node_l4_1.evs
load node_l4_2.evs
load node_l4_3.evs
load node_l4_4.evs

i1 = find(node_l4_1(:,6) == 1);
plot(node_l4_1(i1,1)/1e9,1*ones(1,length(i1)),'ob')
hold on

i2 = find(node_l4_2(:,6) == 1);
plot(node_l4_2(i2,1)/1e9,2*ones(1,length(i2)),'*r')

i3 = find(node_l4_3(:,6) == 1);
plot(node_l4_3(i3,1)/1e9,3*ones(1,length(i3)),'+g')

i4 = find(node_l4_4(:,6) == 1);
plot(node_l4_4(i4,1)/1e9,4*ones(1,length(i4)),'^k')
hold off

axis([0 0.45 0 5])