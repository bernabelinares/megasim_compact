.netlist
source {in0} moving_plus_wnoise.stim

module_conv_plastic {in0,out_post,dw1}{out_post} plastic_conv_module.prm simple1.stt
module_stdp_orderbased {in0,out_post}{dw1} stdp_orderbased.prm simple1.stt

.options
Tmax = 36727693
