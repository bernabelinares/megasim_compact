.netlist
source {in0} nmoving_plus_nnoise.stim

module_conv_plastic {in0,out_post,dw1}{out_post} plastic_conv_module.prm simple1.stt
module_stdp {in0,out_post}{dw1} stdp_flat.prm simple1.stt

.options
Tmax = 559005773
