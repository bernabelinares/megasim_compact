import numpy as np
import pylab as plt

INSIZE=32
MAXSIZE=128

inputspikes=np.genfromtxt("node_in0.evs",dtype="int")
out1=np.genfromtxt("node_out_post.evs",dtype="int")


out1[:,3]+=INSIZE

#out2[:,4]+=INSIZE
allspikes=np.vstack(([inputspikes,out1]))
allspikes=np.sort(allspikes.view('i8,i8,i8,i8,i8,i8'), order=['f0'], axis=0).view(np.int)

np.savetxt("merged_spikes.txt",allspikes,delimiter=" ",fmt="%d")

# hist2d=np.zeros((MAXSIZE,MAXSIZE))

# for i in range(len(allspikes)):
# 	hist2d[allspikes[i,4],allspikes[i,3]]+=1

# plt.imshow(hist2d,interpolation="none",cmap=plt.cm.gray_r)
# plt.colorbar()
# plt.show()
