import numpy as np
import pylab as plt

NUM_POST_NEURONS = 1
NUM_PRE_NEURONS = 10
runtime=3500

w=np.genfromtxt("node_dw1.W",delimiter=" ",dtype="int")
prespikes = np.genfromtxt("node_in0.evs",delimiter=" ",dtype="int")
postspikes=np.genfromtxt("node_out_post.evs",delimiter=" ",dtype="int")

fig2=plt.figure()
sp = plt.subplot2grid((2,2), (0, 0))
z=np.asarray([x for x in prespikes if x[0]<28])
o = np.asarray([x for x in postspikes if x[0]<30])

plt.scatter(prespikes[0:10][:,0],prespikes[0:10][:,3],color='b')
plt.scatter(o[0][0],NUM_PRE_NEURONS,color='r')
plt.vlines(o[0][0],0,NUM_PRE_NEURONS,color='r')
sp.set_xlabel("time (ms)")
sp.set_ylabel("Neuron ID")
sp.set_xticks(np.linspace(0, 30, 4))
sp.set_xlim(5, 35)
sp.set_title("First repetition")    

sp = plt.subplot2grid((2,2), (0, 1))

z=[[x[0],x[3]] for x in prespikes[-11:len(prespikes)]]
o=[x[1] for x in postspikes if x[0]>3400][-1]

plt.scatter([x[0] for x in z],[x[1] for x in z],color='b')
plt.scatter(o,NUM_PRE_NEURONS,color='r')
plt.vlines(o,0,NUM_PRE_NEURONS,color='r')

sp.set_title("Last repetition")
sp.set_xlabel("time (ms)")
sp.set_ylabel("Neuron ID")
sp.set_xticks(np.linspace(runtime-95, runtime-55, 3))
plt.xlim(runtime-95, runtime-65)

sp = plt.subplot2grid((2,2), (1, 0), colspan=2)
outputspikes = postspikes[:,0]#np.asarray([x[1] for x in portsAndSpikes if x[0]==1])
inputspikes0 = np.asarray([x[0] for x in prespikes if x[3]==0])
latency=outputspikes-inputspikes0

sp.set_xlabel('time (ms)')
sp.set_ylabel('Latency from first input (ms)')
sp.set_title('Latency')
sp.set_ylim(0,20)
sp.set_yticks(np.linspace(0,20,5))
plt.plot(inputspikes0, latency)

plt.subplots_adjust(hspace=.40, wspace=.35, bottom=.08, top=.95, left=.10, right=.95)


plt.show()