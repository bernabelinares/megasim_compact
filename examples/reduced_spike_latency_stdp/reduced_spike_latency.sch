.netlist
source {in0} prespikes.stim

module_conv_plastic {in0,dw1}{out_post} post_synaptic_neuron.prm initial_states.stt
module_stdp {in0,out_post}{dw1} stdp_bench_be.prm initial_states.stt

.options
Tmax = 1200000
