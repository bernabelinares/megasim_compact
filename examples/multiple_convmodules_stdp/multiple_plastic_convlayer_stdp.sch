.netlist
source {in0} nmoving_nnoise.stim

module_conv_plastic {in0,out_post1,out_post2,dw1}{out_post1} stdp_conv_postI1.prm simple1.stt
module_conv_plastic {in0,out_post2,out_post1,dw2}{out_post2} stdp_conv_postI2.prm simple1.stt

module_stdp {in0,out_post1}{dw1} stdp.prm simple1.stt
module_stdp {in0,out_post2}{dw2} stdp.prm simple1.stt

.options
Tmax = 601489999
