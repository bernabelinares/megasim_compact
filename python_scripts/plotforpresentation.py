import numpy as np
import pylab as plt
import math

PLOTASFLOATS=False


def STDPDoubleExp(dt,Ap,Am,taup,taum):
    '''
    '''
    if dt<0:
        f_dt=Ap*math.exp(dt/taup)
    else:
        f_dt=-Am*math.exp(-dt/taum)
    return f_dt
    
def pieceWiseDoubleExpSTDP(xmin,xmax,numofpoints,Ap,Am,taup,taum):
    """
    This function precalculates the double exponential stdp rule and
    returns a numpy array where first column is dt and y column is dw
    
    in python to generate parameter file
    """
    xm=xmin
    xp=xmax
    steps=numofpoints
    # Generate data    
    # we need one more point close to zero for the negative case
    # to compute the linear interpolation correctly
    x_1 = np.linspace(xm, -0.01, steps/2.0)
    x_2 = np.linspace(0, xp, steps/2.0)
    x_1=[x for x in x_1]
    for i in x_2:
        x_1.append(i)

    x=x_1
    y=[STDPDoubleExp(i,Ap,Am,taup,taum) for i in x]

    stdpDoubleExpLUT = np.zeros((len(y),2))
    stdpDoubleExpLUT[:,0]=x
    stdpDoubleExpLUT[:,1]=y
    return stdpDoubleExpLUT

def linearInterpolation(data,dt):
    """
    this will be in the c side
    """
    # if dt outside of the piecewise dt return 0
    # 4 x tau = 0
    if (dt>data[:,0][-1]) or (dt<data[:,0][0]):
        return 0
    
    
    if dt<0:
        i2= np.argmax(data[:,0]>dt)
        i1=i2-1
    else:
        i1= np.argmax(data[:,0]>dt)-1
        i2=i1+1

    if i1>=len(data):
        i1=i1-1

    if i2>=len(data):
        i2 = len(data)-1
        i1 = i2-1

    
    #Calculate the slope on this interval by
    #(y_values[i2]-y_values[i1])/(x_values[i2]-x_values[i1]) (ie dy/dx).
    x1,y1=data[i1,0],data[i1,1]
    x2,y2=data[i2,0],data[i2,1]
    slope=(y2-y1)/(x2-x1)
    #The value at x is now the value at x1 plus the slope multiplied by the distance from x1.
    intepol_= y1 + slope * (dt-x1)
    return intepol_

taup=10
taum=10
fromdt=-(5*taup)
todt=5*taum
A_minus = 0.005
A_plus =  0.00537

WMAX = 30678337
dpoints=20
maxvalue=10.0
intt=WMAX
Apint = int(WMAX*A_plus)
Amint = int(WMAX*A_minus)



stdpKernel=pieceWiseDoubleExpSTDP(fromdt,todt,dpoints,Apint,Amint,taup,taum)
stdpKernel=stdpKernel.astype("int")
np.savetxt("stdpKernelInt%dp_Ap%.5f_Am%.5f_tp%d_tm%d.txt"%(dpoints,A_plus,A_minus,taup,taum),
	stdpKernel.transpose(),delimiter=" ",fmt="%d")

stdpKernelThorpes=np.array(([-50,-153391],[-11,-153391],[-11,163174],[0,163174],[0,-153391],[50,-153391]))

dt=np.arange(fromdt,todt,.1)
dw=[STDPDoubleExp(x,A_plus,A_minus,taup,taum) for x in dt]
#Scale dw so they have the same axis as the piecewise

if not PLOTASFLOATS:
	for i in range(len(dw)):##
		dw[i]=dw[i]*intt

dwpiece = [linearInterpolation(stdpKernel,x) for x in dt]
dwthorpe = [linearInterpolation(stdpKernelThorpes, x)for x in dt]
if PLOTASFLOATS:
	for i in range(len(dwpiece)):##
		dwpiece[i]=dwpiece[i]/float(intt)
	stdpKernel[:,1]/=float(intt)



fig=plt.figure()
ax=fig.add_subplot(111)
#ax.plot(dt,dw,label="Double exp STDP")
ax.set_xlabel("dt",size=15)
ax.set_ylabel("dw",size=15)

ax.plot(dt,dwpiece,color='r')
ax.scatter(stdpKernel[:,0],stdpKernel[:,1],c='k',label="Piecewise STDP A: %d"%dpoints)
ax.scatter(stdpKernelThorpes[:,0],stdpKernelThorpes[:,1],c='b',label="Piecewise STDP B: %d"%len(stdpKernelThorpes))
ax.plot(dt,dwthorpe,color='b')

#ax.set_ylim((-A_minus,A_plus))
ax.set_xlim((fromdt,todt))
plt.legend()
plt.show()
