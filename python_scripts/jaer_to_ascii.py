#!/opt/local/bin/python
#AER-DAT2.0

#AER-DAT2.0
# This is a raw AE data file - do not edit
# Data format is int32 address, int32 timestamp (8 bytes total), repeated for each event
# Timestamps tick is 1 us
# created Tue Apr 29 11:36:59 CEST 2008

#version2 =

#     2

#Addresses are 32 bit

import numpy as np
import sys

if len(sys.argv)<2:
	print "please enter a jAER file"
	sys.exit()

retinaSizeX=128; 
xshift=1
yshift=8
polmask=1


#CIN = np.genfromtxt(sys.argv[1],delimiter=" ",dtype="int")
s=sys.argv[1].split('.')[0]
print "converting %s to %s"%(sys.argv[1],s+".txt")

f = open(sys.argv[1],'r')
for i in range(5):
	f.readline()
All = np.fromfile(f, dtype='>i4')
All = np.transpose(np.reshape(All,(All.shape[0]/2 , 2)))
AllTs = np.uint32(All[1])
#AllTs = AllTs.astype(float)/1000.
AllAddr = np.uint32(All[0])

xmask = 0xfe #hex2dec ('fE')  x are 7 bits (64 cols) ranging from bit 1-7
ymask = 0x7f00 #hex2dec ('7f00')  y are also 7 bits ranging from bit 8 to 14.
xshift=1 # bits to shift x to right
yshift=8 # bits to shift y to right
polmask=1 # polarity bit is LSB


pol= (AllAddr & polmask) # 0 is on, 1(Polirity = -1) is off 
AllAddr = AllAddr + pol
x=(AllAddr & xmask) >> xshift
x=(retinaSizeX-1)-x
y=(AllAddr & ymask) >> yshift

# change polarities to megasim format 1 is on, -1 is off
pol=pol.astype("int")
pospol=np.where(pol==0)[0]
negpol=np.where(pol==1)[0]
pol[pospol]=1
pol[negpol]=-1

CIN=np.zeros((len(x),6),dtype="int")
CIN[:,0]=AllTs
CIN[:,1]-=1
CIN[:,2]-=1
CIN[:,3]=x
CIN[:,4]=y
CIN[:,5]=pol
np.savetxt(s+".txt",CIN,delimiter=" ",fmt="%d")
