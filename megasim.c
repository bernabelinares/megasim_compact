/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco
 
 */

#include "megasim.h"

int max(int a,int b)
{
    return((a>b)?(a):(b));
}

int min(int a,int b)
{
    return((a<b)?(a):(b));
}

int write_event(int time_write,int *ev, node *onode, int n_repeat, int delay_to_repeat) //writes event to output ports and returns latest node_busy among output nodes
{
    int i_end,j,k,time_to_write,i;
    
    for(k=0;k<n_repeat;k++){
        if((onode->nev_pending)++ == MAX_EVENTS_IN_BUFFER_PEND){
            printf("Error in write_event: limit reached: nev_pending>=MAX_EVENTS_IN_BUFFER_PEND\n\n");
            exit(26);
        }
        i_end = onode->index_end;
        i_end++;
        if(i_end == MAX_EVENTS_IN_BUFFER_PEND)
            i_end=0;
        if((i_end == onode->index_start) && (onode->nev_pending !=1)){
            printf("Error in write_event: i_end collides with index_start when filling pending buffer\nNeed larger pending buffer?\n\n");
            exit(27);
        }
        onode->index_end = i_end;
        time_to_write = time_write + k*delay_to_repeat;
        j = i_end-1;
        if(j<0)
            j = MAX_EVENTS_IN_BUFFER_PEND -1;
        //if new event is older than queued events, find its position in the queue and determine how many events i to shift up
        i=0;
        while((onode->nev_pending > 1) && (time_to_write < onode->ev_pending[j][0])){
            j--;
            i++;
            if(j < 0)
                j = MAX_EVENTS_IN_BUFFER_PEND-1;
            if(j == i_end){
                printf("Error in write_event: reached i_end in pending buffer\n\n");
                exit(28);
            }
        }
        
        if(j != ((i_end==0)?(MAX_EVENTS_IN_BUFFER_PEND-1):(i_end-1))){
            move_i_events_up(onode,i);
        }
        if(j != MAX_EVENTS_IN_BUFFER_PEND-1){
            onode->ev_pending[j+1][0] = time_to_write;
            onode->ev_pending[j+1][1] = -1;
            onode->ev_pending[j+1][2] = -1;
            for(i=0;i<EVENT_COMPS;i++)
                onode->ev_pending[j+1][i+3] = ev[i];
        }
        else{
            onode->ev_pending[0][0] = time_to_write;
            onode->ev_pending[0][1] = -1;
            onode->ev_pending[0][2] = -1;
            for(i=0;i<EVENT_COMPS;i++)
                onode->ev_pending[0][i+3] = ev[i];
        }
    }
    onode->t_pre_rqst_latest = onode->ev_pending[onode->index_end][0];
    if(onode->t_pre_rqst_earliest_pending == -1)
        onode->t_pre_rqst_earliest_pending = onode->ev_pending[onode->index_end][0];
    else
        onode->t_pre_rqst_earliest_pending = onode->ev_pending[onode->index_start][0];
    
    //check t_pre_rqst earliest & latest
    check_node_times(onode);
    
    return onode->t_node_busy;
}


int prelim_handshake(int time_act, int *t_ack, int *ev, node* inode, int *time_module_busy, int delay_to_process, int delay_to_ack, int fifo_depth)
{
    int i,i_start;
    
    if(time_act >= *time_module_busy){ //will not use fifo
        *time_module_busy = time_act + delay_to_process;
        if(fifo_depth>0)
            *t_ack = time_act + delay_to_ack;
        else
            *t_ack = time_act + delay_to_process;
    }else{  //will use next position of fifo
        if(*time_module_busy - time_act > fifo_depth*delay_to_process){
            inode->n_unprocessed_evs_because_busy++;
            //inode->flag_unprocessed_evs_because_busy = 1;
            inode->prelim_t_node_busy = max(inode->prelim_t_node_busy,time_act + delay_to_process);
            return 1; //ALERT: cannot process this event because module is busy and fifo depth has been reached.
            //Event will be ignored and its processing should be post-poned for some future time.
        }
        *time_module_busy += delay_to_process;
        if(*time_module_busy - time_act < fifo_depth*delay_to_process)
            *t_ack = time_act + delay_to_ack; //fifo will not be filled with present input event
        else
            *t_ack = max((*time_module_busy)-delay_to_process,time_act+delay_to_ack); //fifo limit reached with present input event
    }
    inode->n_temp_prelim_acks++;
    inode->prelim_ack = max(inode->prelim_ack,*t_ack);
    
    //get pending input event
    i_start = inode->index_start;
    for(i=0;i<EVENT_COMPS;i++)
        ev[i] = inode->ev_pending[i_start][3+i];
    
    return 0;
}

int move_i_events_up(node* nod,int i)
{
    int j,i_end,k1,k2,m;
    
    i_end = nod->index_end;
    k1 = i_end+1;
    k2 = i_end;
    for(j=0;j<i;j++){
        k1--;
        if(k1<0)
            k1 = MAX_EVENTS_IN_BUFFER_PEND-1;
        k2--;
        if(k2<0)
            k2 = MAX_EVENTS_IN_BUFFER_PEND-1;
        nod->ev_pending[k1][0] = nod->ev_pending[k2][0];
        nod->ev_pending[k1][1] = nod->ev_pending[k2][1];
        nod->ev_pending[k1][2] = nod->ev_pending[k2][2];
        for(m=0;m<EVENT_COMPS;m++)
            nod->ev_pending[k1][m+3] = nod->ev_pending[k2][m+3];
    }
    
    return(0);
}
int check_node_times(node* nod)
{
    if(nod->ev_pending[nod->index_start][0] != nod->t_pre_rqst_earliest_pending){
        printf("Error 1 in check_node_times: ");
        printf("nod->ev_pending[nod->index_start][0] (%d) != nod->t_pre_rqst_earliest_pending (%d)\n\n",nod->ev_pending[nod->index_start][0],nod->t_pre_rqst_earliest_pending);
        exit(29);
    }else
        if((nod->ev_pending[nod->index_end][0] != nod->t_pre_rqst_latest) && (nod->nev_pending > 0)){
            printf("Error 2 in check_node_times: ");
            printf("nod->ev_pending[nod->index_end][0] (%d) != nod->t_pre_rqst_latest (%d)\n\n",nod->ev_pending[nod->index_end][0],nod->t_pre_rqst_latest);
            exit(30);
        } else
            if((nod->index_end == -1) && (nod->nev_pending != 0)){
                printf("Error 3 in check_node_times: ");
                printf("(nod->index_end == -1) && (nod->nev_pending != 0)\n\n");
                exit(31);
            }else
                if(((nod->index_start == 0) && (nod->index_end == MAX_EVENTS_IN_BUFFER_PEND - 1)) && ((nod->nev_pending != MAX_EVENTS_IN_BUFFER_PEND) && (nod->nev_pending != 0))){
                    printf("Error 4 in check_node_times: ");
                    printf("nod->index_start == 0 && nod->index_end == MAX_EVENTS_IN_BUFFER_PEND - 1, but nev_pending is neither 0 nor MAX\n\n");
                }
    
    return 0;
}




int init_N_prelim_acks(node* nodes,int total_nodes,int total_modules,ntw** node2modin)
{
    int i,j,k,q,fan_out;
    
    for(i=0;i<total_nodes;i++){
        fan_out=0;
        for(j=0;j<total_modules;j++){
            k=0;
            while((q=node2modin[i][j].ports[k]) != -1){
                k++;
                fan_out++;
            }
        }
        nodes[i].N_prelim_acks = fan_out;
    }
    
    return 0;
}



int check_if_ok(int* ok, char* param_name, char* file_name){
    
    if(*ok == 1){
        printf("Error in parameter file %s: parameter %s read twice\n\n",file_name,param_name);
        exit(73);
    }
    else
        *ok = 1;
				
    return 0;
}

int get_node_index(char* nname,char** node_names,int total_nodes)
{
    int i;
    
    for(i=0;i<total_nodes;i++)
        if(strcmp(nname,node_names[i]) == 0)
            return i;
    printf("Error in get_node_index. Node name %s not found in list of node names\n\n",nname);
    exit(74);
}


int handshake(int time_act, node* inode, int final_ack)
{
    int i,j,i_start, t_pre_rqst_tmp, t_pre_rqst, t_rqst, t_ack, ev_x, ev_y, ev_s;
    int nev_pending, t_pre_rqst_latest, index_end;
    int ev[EVENT_COMPS];
    
    t_pre_rqst_tmp=inode->t_pre_rqst_earliest_pending;
    if(t_pre_rqst_tmp > time_act){
        printf("Error in handshake: t_pre_rqst_earliest_pending>time_act\n\n");
        exit(32);
    }
    
    i_start = inode->index_start;
    
    t_pre_rqst = inode->ev_pending[i_start][0];
    if(t_pre_rqst != t_pre_rqst_tmp){
        printf("Error in handshake: t_pre_rqst (%d) != t_pre_rqst_tmp (%d)\n\n",t_pre_rqst,t_pre_rqst_tmp);
        exit(33);
    }
    inode->ev_pending[i_start][0] = -1; //optional: set to -1 to indicate event will be passed to consolidated buffer
    
    t_rqst = inode->ev_pending[i_start][1];
    if(t_rqst != -1){
        printf("Error in handshake: t_rqst != -1\n\n");
        exit(34);
    }
    t_rqst = time_act;
    
    if(inode->ev_pending[i_start][2] != -1){
        printf("Error in handshake: t_ack != -1\n\n");
        exit(35);
    }
    
    inode->t_node_busy = final_ack;
    
    for(i=0;i<EVENT_COMPS;i++)
        ev[i] = inode->ev_pending[i_start][3+i];
    
    //erase handshaked event from input port node pending buffer and update t_pre_rqst_earliest_pending:
    if(i_start == MAX_EVENTS_IN_BUFFER_PEND-1){
        inode->index_start = 0;
        inode->t_pre_rqst_earliest_pending = inode->ev_pending[0][0];
    }
    else{
        inode->index_start = i_start+1;
        inode->t_pre_rqst_earliest_pending = inode->ev_pending[i_start+1][0];
    }
    inode->nev_pending = (inode->nev_pending)-1;
    if(inode->nev_pending < 0){
        printf("Error in handshake of function handshake: nev_pending<0\n\n");
        exit(36);
    }
    
    //add handshaked event to inport port node consolidated buffer:
    i = inode->nev_cons;
    i++;
    inode->nev_cons = i;
    inode->ev_consolidated[i-1][0] = t_pre_rqst;
    inode->ev_consolidated[i-1][1] = t_rqst;
    inode->ev_consolidated[i-1][2] = final_ack;
    for(j=0;j<EVENT_COMPS;j++)
        inode->ev_consolidated[i-1][3+j]=ev[j];
    // If consolidated buffer is full, save consolidated events to file, except the last one
    if(i == MAX_EVENTS_IN_BUFFER_CONS){
        if(inode->f_out == NULL){
            inode->f_out = fopen(inode->node_ofile_name,"w");
        }
        for(i=0;i<MAX_EVENTS_IN_BUFFER_CONS-1;i++){
            for(j=0;j<EVENT_COMPS+3;j++)
                fprintf(inode->f_out," %d",inode->ev_consolidated[i][j]);
            fprintf(inode->f_out,"\n");
        }
        for(j=0;j<EVENT_COMPS+3;j++)
            inode->ev_consolidated[0][j] = inode->ev_consolidated[MAX_EVENTS_IN_BUFFER_CONS-1][j];
        inode->nev_cons = 1;
    }
    
    //if pending buffer empty and node is stimulus, refill from stimulus file
    if(inode->nev_pending == 1 && inode->is_stimulus == 1){
        nev_pending = 1;
        t_pre_rqst_latest = inode->t_pre_rqst_latest;
        index_end = inode->index_end;
        while((nev_pending < MAX_EVENTS_IN_BUFFER_PEND) && (fscanf(inode->f_stim,"%d %d %d %d %d %d",&t_pre_rqst,&t_rqst,&t_ack,&ev_x,&ev_y,&ev_s) != EOF)){
            nev_pending++;
            index_end++;
            if(index_end == MAX_EVENTS_IN_BUFFER_PEND)
                index_end=0;
            inode->ev_pending[index_end][0] = t_pre_rqst;
            inode->ev_pending[index_end][1] = t_rqst;
            inode->ev_pending[index_end][2] = t_ack;
            inode->ev_pending[index_end][3] = ev_x;
            inode->ev_pending[index_end][4] = ev_y;
            inode->ev_pending[index_end][5] = ev_s;
            if(t_pre_rqst_latest < t_pre_rqst)
                t_pre_rqst_latest = t_pre_rqst;
        }
        inode->nev_pending = nev_pending;
        inode->t_pre_rqst_latest = t_pre_rqst_latest;
        inode->index_end = index_end;
    }
    
    //check t_pre_rqst earliest & latest
    
    check_node_times(inode);
    
    return 0;
}



int first_read_netlist(FILE* fsch, int* total_nodes, int* total_modules, int* T_max,char*** p, char*** pp)
{
    int i, j, k, last, size=1024, pos, c, li=0, lj=0, netlist_line=-1, options_line=-1, total_lines=0, error = 1, line_empty, max_nodes = 100;
    
    char **array, **array_netlist, **node_names, **node_names_tmp, node_name_tmp[MAX_NODE_NAME+1];
    char aa[SCHEMATICFILE_MAX_LINE_LENGTH+1], bb[SCHEMATICFILE_MAX_LINE_LENGTH+1];
    
    //copy sch file in buffer and determine total number of lines in file
    char *buffer = malloc(size);
    pos = 0;
    if(fsch){
        do{
            c = fgetc(fsch);
            if(c != EOF) buffer[pos++] = c;
            if(pos >= size - 1) { // increase buffer length - leave room for 0
                size *=2;
                buffer = (char*)realloc(buffer, size);
            }
            if(c == '\n'){
                total_lines++;
                lj = pos-1;
                if(lj-li > SCHEMATICFILE_MAX_LINE_LENGTH){
                    printf("Error: line length %d of line %d seems to be large than %d\n\n",lj-li,total_lines,SCHEMATICFILE_MAX_LINE_LENGTH);
                    exit(11);
                }
                li = lj;
            }
        }while(c != EOF);
    }
    else{
        printf("Error in file pointer in first_read_netlist\n\n");
        exit(12);
    }
    
    //copy lines in 2D char array "array"
    array = malloc(total_lines * sizeof(char *));
    for(i=0;i<total_lines;i++)
        array[i] = malloc((SCHEMATICFILE_MAX_LINE_LENGTH+1) * sizeof(char));
    pos = 0;
    for(i=0;i<total_lines;i++){
        j=0;
        do{
            c = buffer[pos++];
            array[i][j++] = c;
        }while(c != '\n');
        array[i][j-1] = '\0';
    }
    free(buffer);
    
    for(i=0;i<total_lines;i++){
        if(strcmp(array[i],".netlist") == 0)
            netlist_line = i;
        if(strcmp(array[i],".options") == 0)
            options_line = i;
    }
    if(netlist_line>=options_line){
        printf("Please use .netlist before .options in .sch input file\n\n");
        exit(13);
    }
    
    // Read T_max
    for(i=options_line+1;i<total_lines;i++)
        if(sscanf(array[i]," Tmax = %d\n",T_max) == 1){
            if(error == 0){
                printf("It looks like Tmax appears twice in input file\n\n");
                exit(14);
            }
            else
                error = 0;
        }
				
    // Extract netlist lines and number of modules
    array_netlist = malloc((options_line - netlist_line - 1) * sizeof(char *));
    for(i=0;i<options_line - netlist_line - 1;i++){
        array_netlist[i] = malloc((SCHEMATICFILE_MAX_LINE_LENGTH+1) * sizeof(char));
    }
    total_modules[0] = -1;
    for(i = netlist_line+1;i<options_line;i++){
        line_empty = 1;
        j = 0;
        while((c = array[i][j++]) != '\0'){
            if(c != ' ' && c != '\0' && c != '\n')
                line_empty = 0;
            if(j == SCHEMATICFILE_MAX_LINE_LENGTH+1){
                printf("Error: end of line string without \\0 char\n\n");
                exit(15);
            }
        }
        if(line_empty == 0){
            total_modules[0]++;
            j = 0;
            while((c = array[i][j++]) != '\0')
                array_netlist[total_modules[0]][j-1] = c;
            array_netlist[total_modules[0]][j-1] = c;
        }
    }
    if(total_modules[0] == -1){
        printf("Error: no modules found in input file\n\n");
        exit(16);
    }
    (total_modules[0])++;
    *p = array_netlist;
    
    // Obtain number of nodes
    total_nodes[0] = -1;
    node_names = malloc(max_nodes * sizeof(char *));
    for(i=0;i<max_nodes;i++)
        node_names[i] = malloc((MAX_NODE_NAME+1) * sizeof(char *));
    for(i=0;i<total_modules[0];i++){
        j = 0;
        while(((c = array_netlist[i][j++]) != '{') && (c != '\0'));
        if(c == '\0'){
            printf("Error in line: %s\n  No left bracket { found\n\n",array_netlist[i]);
            exit(17);
        }
        else{
            k = 0;
            while(((c = array_netlist[i][j++]) != '}') && (c != '\0'))
                aa[k++] = c;
            aa[k] = '\0';
            if(c == '\0'){
                printf("Error in line: %s\n  Right bracket } missing\n\n",array_netlist[i]);
                exit(18);
            }
            while(((c = array_netlist[i][j++]) != '{') && (c != '\0'));
            if(c == '\0')
                bb[0] = '\0';
            else{
                k = 0;
                while(((c = array_netlist[i][j++]) != '}') && (c != '\0'))
                    bb[k++] = c;
                bb[k] = '\0';
                if(c == '\0'){
                    printf("Error in line: %s\n  Right bracket } missing\n\n",array_netlist[i]);
                    exit(19);
                }
            }
        }
        last = 0;
        j = 0;
        while(last == 0){
            k = 0;
            while(((c = aa[j++]) != ',') && (c != '\0'))
                node_name_tmp[k++] = c;
            if(c == '\0')
                last = 1;
            node_name_tmp[k] = '\0';
            if(strlen(node_name_tmp) != 0 && new_node_name(node_name_tmp,node_names,total_nodes[0]) == 1){
                if(total_nodes[0] == max_nodes-1){
                    max_nodes = max_nodes * 2;
                    node_names_tmp = realloc(node_names, max_nodes * sizeof(char *));
                    if(!node_names_tmp){
                        printf("Error reallocating memory for node_names when max_nodes becomes %d\n\n",max_nodes);
                        exit(20);
                    }
                    else
                        node_names = node_names_tmp;
                    for(k=max_nodes/2;k<max_nodes;k++)
                        node_names[k] = malloc((MAX_NODE_NAME+1) * sizeof(char *));
                }
                (total_nodes[0])++;
                strcpy(node_names[total_nodes[0]],node_name_tmp);
            }
        }
        last = 0;
        j = 0;
        while(last == 0){
            k = 0;
            while(((c = bb[j++]) != ',') && (c != '\0'))
                node_name_tmp[k++] = c;
            if(c == '\0')
                last = 1;
            node_name_tmp[k] = '\0';
            if(strlen(node_name_tmp) != 0 && new_node_name(node_name_tmp,node_names,total_nodes[0]) == 1){
                if(total_nodes[0] == max_nodes-1){
                    max_nodes = max_nodes * 2;
                    node_names_tmp = realloc(node_names, max_nodes * sizeof(char *));
                    if(!node_names_tmp){
                        printf("Error reallocating memory for node_names when max_nodes becomes %d\n\n",max_nodes);
                        exit(21);
                    }
                    else
                        node_names = node_names_tmp;
                    for(k=max_nodes/2;k<max_nodes;k++)
                        node_names[k] = malloc((MAX_NODE_NAME+1) * sizeof(char *));
                }
                (total_nodes[0])++;
                strcpy(node_names[total_nodes[0]],node_name_tmp);
            }
        }
    }
    if(total_nodes[0] == -1){
        printf("Error: no nodes found in input file\n\n");
        exit(22);
    }
    total_nodes[0]++;
    *pp = node_names;
    
    return 0;
}

int new_node_name(char *node_name_tmp, char **node_names, int nodes){
    int i;
    
    if(nodes == -1) //this is first node, so it is new
        return 1;
    for(i=0;i<=nodes;i++)
        if(strcmp(node_name_tmp,node_names[i]) == 0) //this node name matches a previous one, so it is not new
            return 0;
    return 1; //this node name did not match any previous one, so it is new
}

int getintstream(ntw** netw, int index1, int index2, int* out)
{
    int i,j,a,l=0,lt,k=0;
    
    lt = 2*index1*index2;
    for(i=0;i<index1;i++)
        for(j=0;j<index2;j++){
            k=0;
            while((a = netw[i][j].ports[k]) != -1){
                if(l == lt){
                    lt += lt;
                    out = realloc(out,lt * sizeof(int));
                }
                out[l] = a;
                l++;
                k++;
            }
            if(l == lt){
                lt += lt;
                out = realloc(out,lt * sizeof(int));
            }
            out[l] = a;
            l++;
        }
    out = realloc(out,l * sizeof(int));
    return(l);
}

int getntw(ntw** netw, int index1, int index2, int* ntwstream, int ntwstream_length)
{
    int i,j,k=0,l=0,m,a,pp[MAX_IN_PORTS + MAX_OUT_PORTS];
    for(i=0;i<index1;i++)
        for(j=0;j<index2;j++){
            k=0;
            while((a=ntwstream[l++]) != -1){
                pp[k++] = a;
            }
            pp[k++] = -1;
            netw[i][j].ports = realloc(netw[i][j].ports,k * sizeof(int));
            for(m=0;m<k;m++)
                netw[i][j].ports[m] = pp[m];
        }
    if(l != ntwstream_length){
        printf("Error in ntwstream_length in getntw\n\n");
        exit(23);
    }
    return(0);
}

int find_earliest_pendings_and_busy(int total_nodes,node* nodes,int *list_of_nodes)
{
    int i,j=0, tmin = -1, tmax;
    
    for(i=0;i<total_nodes;i++)
        if(nodes[i].t_pre_rqst_earliest_pending != -1)
            if(((tmax=max(nodes[i].t_pre_rqst_earliest_pending,nodes[i].t_node_busy)) <= tmin) || (tmin == -1)){
                if(tmax == tmin){
                    list_of_nodes[j] =  i;
                    j++;
                }else{
                    list_of_nodes[0] =  i;
                    j=1;
                }
                tmin = tmax;
            }
    list_of_nodes[j] = -1;
    
    return tmin;
}




int save_and_close_node(node* nod)
{
    int i,j,k;
    
    if(nod->f_out == NULL)
        nod->f_out=fopen(nod->node_ofile_name,"w");
    
    for(i=0;i<nod->nev_cons;i++){
        for(j=0;j<EVENT_COMPS+3;j++)
            fprintf(nod->f_out," %d",nod->ev_consolidated[i][j]);
        fprintf(nod->f_out,"\n");
    }
    
    // We better don't print out the pending events
    k=nod->index_start;
    for(i=0;i<nod->nev_pending;i++){
        for(j=0;j<EVENT_COMPS+3;j++)
            fprintf(nod->f_out," %d",nod->ev_pending[k][j]);
        fprintf(nod->f_out,"\n");
        k++;
        if(k > MAX_EVENTS_IN_BUFFER_PEND-1)
            k=0;
    }
    
    fclose(nod->f_out);
    nod->f_out = NULL;
    if(nod->is_stimulus == 1){
        fclose(nod->f_stim);
        nod->f_stim = NULL;
    }
    
    return(0);
}

int init_node_0(node *nodes)
{
    int j;
    
    nodes[0].t_pre_rqst_latest = 90;
    nodes[0].t_pre_rqst_earliest_pending = 0;
    nodes[0].t_node_busy = 0;
    nodes[0].nev_cons = 0;
    nodes[0].nev_pending = 10;
    nodes[0].index_start = 0;
    nodes[0].index_end = 9;
    for(j=0;j<10;j++){
        nodes[0].ev_pending[j][0]=j*10;
        nodes[0].ev_pending[j][3]=j+1; //x
        nodes[0].ev_pending[j][4]=j+2; //y
        nodes[0].ev_pending[j][5]=((j/2)*2 ==j) ? (1):(-1); //sign
    }
    
    return(0);
}

int init_stimulus_node(node *nodes,int node_number,char* s)
{
    int t_pre_rqst_latest=0,nev_pending=0;
    int t_pre,t_req,t_ack,x,y,sign;
    int offset=0,i,tt;
    FILE *fp;
    
    if((fp = fopen(s,"r")) == NULL){
        printf("File %s does not exist\n",s);
        exit(37);
    }
    nodes[node_number].f_stim = fp;
    strcpy(nodes[node_number].stimulus_file_name,s);
    
    while((nev_pending < MAX_EVENTS_IN_BUFFER_PEND) && (fscanf(fp,"%d %d %d %d %d %d",&t_pre,&t_req,&t_ack,&x,&y,&sign)) != EOF){
        nodes[node_number].ev_pending[nev_pending+offset][0] = t_pre;
        nodes[node_number].ev_pending[nev_pending+offset][1] = t_req;
        nodes[node_number].ev_pending[nev_pending+offset][2] = t_ack;
        nodes[node_number].ev_pending[nev_pending+offset][3] = x;
        nodes[node_number].ev_pending[nev_pending+offset][4] = y;
        nodes[node_number].ev_pending[nev_pending+offset][5] = sign;
        if(t_pre_rqst_latest < t_pre)
            t_pre_rqst_latest = t_pre;
        nev_pending++;
    }
    tt = t_pre_rqst_latest;
    for(i=0;i<nev_pending;i++)
        if(nodes[node_number].ev_pending[i+offset][0] < tt)
            tt = nodes[node_number].ev_pending[i+offset][0];
    nodes[node_number].t_pre_rqst_latest = t_pre_rqst_latest;
    nodes[node_number].t_pre_rqst_earliest_pending = tt;
    nodes[node_number].t_node_busy = 0;
    nodes[node_number].nev_cons = 0;
    nodes[node_number].nev_pending = nev_pending;
    nodes[node_number].index_start = 0+offset;
    nodes[node_number].index_end = nev_pending-1+offset;
    nodes[node_number].is_stimulus = 1;
    
    return(0);
}

int see_nodes(node** some_nodes)
{
    some_nodes[1]->t_node_busy = 10;
    if(some_nodes[3]==(node*) -1)
        printf("%p %p %p %p\n",some_nodes[0],some_nodes[1],some_nodes[2],some_nodes[3]);
    
    return(0);
}

int init_allnodes(node *nodes, int n, char **node_names)
{
    int a, b, j;
    
    for(a=0;a<n;a++){
        nodes[a].node_number = a;
        //sprintf(nodes[a].node_name,"node_%d",a);
        sprintf(nodes[a].node_name,"node_%s",node_names[a]);
        nodes[a].t_pre_rqst_latest = -1;
        nodes[a].t_pre_rqst_earliest_pending = -1;
        nodes[a].prelim_ack = -1;
        nodes[a].N_prelim_acks = -1;
        nodes[a].n_temp_prelim_acks = 0;
        nodes[a].t_node_busy = -1;
        nodes[a].n_unprocessed_evs_because_busy = 0;
        nodes[a].flag_unprocessed_evs_because_busy = 0;
        nodes[a].prelim_t_node_busy = -1;
        nodes[a].nev_cons = 0;
        nodes[a].nev_pending = 0;
        nodes[a].index_start = 0;
        nodes[a].index_end = -1;
        for(j=0;j<MAX_EVENTS_IN_BUFFER_CONS;j++)
            for(b=0;b<COMPS_EV;b++)
                nodes[a].ev_consolidated[j][b]=-1;
        for(j=0;j<MAX_EVENTS_IN_BUFFER_PEND;j++)
            for(b=0;b<COMPS_EV;b++)
                nodes[a].ev_pending[j][b]=-1;
        nodes[a].is_stimulus = 0;
        nodes[a].f_out = NULL;
        nodes[a].f_stim = NULL;
        strcpy(nodes[a].node_ofile_name,strcat(nodes[a].node_name,".evs"));
    }
    
    return 0;
}


