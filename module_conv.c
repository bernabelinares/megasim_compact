/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco
 
 */

#include "megasim.h"

#define NKPARAMS 21
#define NOPARAMS 11
#define KKPARAMS 4

FILE *logStates;
int logStatesFlag = 0;
char logStatesName[40]="";

int ev_post(int z,int crop_zmin,int crop_zmax,int zshift_pre,int z_subsmp,int zshift_pos,int *cropped_z);

//int module_conv(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes,node** onodes,int *params,int *state,double *fparams,double *fstate)
int module_conv(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes,node** onodes,
		prmss *params,prmss *state)

{
	// Event-Driven Convolution module
	//
	// Common params (params required for any module):
	//
	// x+2 parameters in *params (x = 11 + NK) (NK: kernel parameters, one set per input port):
	// n_params int = x : number of int params in module_conv
	// n_params double = 0 : number of double params in module_conv
	// n_in_ports: number of input ports
	// n_out_ports: number of output ports
	// delay_to_process: time to process an input event: will set time_busy as well as t_pre_rqst of output events
	// delay_to_ack: time to acknowledge an input request
	// fifo_depth: number of input events that can be accummulated and processed: time_busy - time_act <= fifo_depth * delay_to_process. If this is not met, then ack should acknowledged late enough to guarantee fifo has space for a next event
	// n_repeat: number of times each input event is repeated at an output port
	// delay_to_repeat: delay of repeating output events
	//
	// module_conv specific params:
	//
	// Nx_array: x-size of 2D integrator array
	// Ny_array: y-size of 2D integrator array
	// THplus: positive threshold level for each pixel
	// THplusInfo: if 1 positive output events will generated. Otherwise not.
	// THminus: negative threshold level for each pixel
	// THminusInfo: if 1 negative output events will generated. Otherwise not.
	// Reset_to_reminder: if 1, membrane is reset to the excess value above/below threshol
	// MembReset: Membrane value at Reset if Reset_to_reminder=0
	// TLplus: positive leakage time from THplus if no input events are received. If TLplus=0, no leakage is applied
	// TLminus: negative leakage time from THminus if no input events are received. If TLminus=0, no leakage is applied
	// Tmin: minimum output events time separation. Defines pixel maximum firing frequency
	// T_Refract: refractory time. After a spike, the pixel remains inactive for T_Refract
	// kernels: each input has its own kernel. For each input the following kernel parameters must be defined:
	// Nx_kernel: x-size of 2D kernel
	// Ny_kernel: y-size of 2D kernel
	// Dx: x displacement of kernel low-left vertice with respect to input event
	// Dy: y displacement of kernel low-left vertice with respect to input event
	// After these 4 params, all (Nx_kernel x Ny_kernel) kernel values are given
	
	// output ports parameters: for each output port we define 10 parameters:
	// xshift_pre: x shift before subsampling
	// yshift_pre: y shift before subsampling
	// x_subsmp: subsampling x factor
	// y_subsmp: subsampling y factor
	// xshift_pos: x shift after subsampling
	// yshift_pos: y shift after subsampling
	// crop_xmin: cropped xmin at output
	// crop_xmax: cropped xmax at output
	// crop_ymin: cropped ymin at output
	// crop_ymax: cropped ymax at output
	//
	// Common state variables (required for each module):
	//
	// 3+n_in_ports+x state variables in *state (x includes 3 (Nx_array x Ny_array) arrays: membrane state, time last in spike, time last out spike):
	// 0: number of int state variables
	// 1: number of float state variables
	// 2: time_busy: time until latest event will finish processing
	// for each input port: state of preliminary ack
	//
	// module_conv specific state variables:
	//
	// states for 2D (Nx_array x Ny_array) array size of pixel membrane state
	// states for 2D (Nx_array x Ny_array) array size of pixel last input spike time (to update leakage since last input event)
	// states for 2D (Nx_array x Ny_array) array size of pixel last output spike time (to check for Tmin condition)
	//
	// time_act: present simulation time. This function will not change it (not a pointer)
	// 
	// inodes: pointer to sub-list of input nodes. 
	// onodes: pointer to sub-list of output nodes.
	// 
	// *ev_in_ports: int vector, one component per input port. Each component indicates if there is an input event at this input port at time time_act (1) or not (0)
	// *ev_out_port: int vector, one component per output port. Each component indicates the number of output events written on this output port at time time_act
	//
	
	int n_in_ports,n_out_ports,n_repeat,delay_to_process,delay_to_ack,delay_to_repeat,fifo_depth,time_busy;
	int in_port,out_port,nin, alert;
	int ev[EVENT_COMPS],evo[EVENT_COMPS];
	int Nx_array, Ny_array, THplus, THplusInfo, THminus, THminusInfo, Reset_to_reminder, MembReset, TLplus, TLminus, Tmin, T_Refract, Xmin, Ymin;
	int Nx_kernel, Ny_kernel, Dx, Dy, offset, x, y, i, j, k, t_last_in_spike, t_last_out_spike;
	int xshift_pre; // x shift before subsampling
	int yshift_pre; // y shift before subsampling
	int x_subsmp; // subsampling x factor
	int y_subsmp; // subsampling y factor
	int xshift_pos; // x shift after subsampling
	int yshift_pos; // y shift after subsampling
	int crop_xmin; // cropped xmin at output
	int crop_xmax; // cropped xmax at output
	int crop_ymin; // cropped ymin at output
	int crop_ymax; // cropped ymax at output
	int cropped_x,cropped_y;
	int rectify; // if 0 do not rectify, if 1 set sign=1, if -1 set sign=-1
	
	n_in_ports       = params->prms[2];
	if(ev_in_ports[n_in_ports] != -1){
		printf("Error in module_conv: ev_in_ports[%d] != -1\n\n",n_in_ports);
		exit(0);
	}
	// Common Module Params
	n_out_ports       = params->prms[3];
	delay_to_process  = params->prms[4]; //affects new time_busy and t_pre_rqst of output events
	delay_to_ack      = params->prms[5];
	fifo_depth        = params->prms[6];
	n_repeat          = params->prms[7];
	delay_to_repeat   = params->prms[8];
	// Params Specific for module_conv
	Nx_array          = params->prms[9];
	Ny_array          = params->prms[10];
	THplus            = params->prms[11];
	THplusInfo        = params->prms[12];
	THminus           = params->prms[13];
	THminusInfo       = params->prms[14];
	Reset_to_reminder = params->prms[15];
	MembReset         = params->prms[16];
	TLplus            = params->prms[17];
	TLminus           = params->prms[18];
	Tmin              = params->prms[19];
	T_Refract         = params->prms[20];
	Xmin				= params->prms[21];
	Ymin				= params->prms[22];
	// kernel params, one set per each input
	
	// output ports params
	offset = state->prms[3 + 2*n_in_ports -1]; //offset value of last kernel
	Nx_kernel = params->prms[offset]; //Nx_kernel of last kernel
	Ny_kernel = params->prms[offset + 1]; //Ny_kernel of last kernel
	offset = offset + KKPARAMS + Nx_kernel*Ny_kernel; // first position after all kernels
	crop_xmin			= params->prms[offset]; // cropped xmin at output
	crop_xmax			= params->prms[offset+1]; // cropped xmax at output
	crop_ymin			= params->prms[offset+2]; // cropped ymin at output
	crop_ymax			= params->prms[offset+3]; // cropped ymax at output
	xshift_pre			= params->prms[offset+4]; // x shift before subsampling
	yshift_pre			= params->prms[offset+5]; // y shift before subsampling
	x_subsmp			= params->prms[offset+6]; // subsampling x factor
	y_subsmp			= params->prms[offset+7]; // subsampling y factor
	xshift_pos			= params->prms[offset+8]; // x shift after subsampling
	yshift_pos			= params->prms[offset+9]; // y shift after subsampling
	rectify			= params->prms[offset+10]; // if 0 do not rectify, if 1 set sign=1, if -1 set sign=-1

	
	

	time_busy = state->prms[2];
	
	if (logStatesFlag){
		logStates = fopen(logStatesName,"a");
		if (logStates==NULL){
			printf("Error could not create/append log file\n");
			exit(11);
		}
	}

	for(in_port=0;in_port<n_in_ports;in_port++){ //process all simultaneous input events on active input ports
		nin = ev_in_ports[in_port]; //'1' if there is input event, '0' otherwise
		if(nin != 0 && nin != 1){printf("Error in module_conv: nin != 0,1\n\n");exit(0);}
		if(nin == 1){
			alert = prelim_handshake(time_act,&(prelim_ack[in_port]),ev,inodes[in_port],&time_busy,delay_to_process,delay_to_ack,fifo_depth);
			if(alert == 0){
				//state[2] = time_busy;
				{

					// Do Event Computations Here
					offset = state->prms[3 + n_in_ports + in_port];
					Nx_kernel = params->prms[offset];
					Ny_kernel = params->prms[offset + 1];
					Dx        = params->prms[offset + 2];
					Dy        = params->prms[offset + 3];
					// Update global events and connections computation counters
					conv_events += 1;
					conv_connects += Nx_kernel*Ny_kernel;
					// continue Event Computations
					for(j=max(0,-(ev[1]-Ymin)-Dy),y=max(0,(ev[1]-Ymin)+Dy);y<min(Ny_array,(ev[1]-Ymin)+Dy+Ny_kernel);j++,y++)
						for(i=max(0,-(ev[0]-Xmin)-Dx),x=max(0,(ev[0]-Xmin)+Dx);x<min(Nx_array,(ev[0]-Xmin)+Dx+Nx_kernel);i++,x++){
							k = 3 + 2*n_in_ports + y*Nx_array + x;
							if(k + 2*Nx_array*Ny_array >= state->prms[0]){
								printf("State out of bound (1)\n\n");
								exit(0);
							}
							t_last_out_spike = state->prms[k + 2*Nx_array*Ny_array];
							if(t_last_out_spike + T_Refract <= time_act){
								t_last_in_spike = state->prms[k + Nx_array*Ny_array];
								state->prms[k + Nx_array*Ny_array] = time_act; // update t_last_in_spike
								if(state->prms[k]>=MembReset){
									if(TLplus != 0)
									state->prms[k] = max(MembReset, state->prms[k]-(int)((((double)(THplus-MembReset))*((double)(time_act - t_last_in_spike)))/((double)TLplus)));
								}
								else
									if(TLminus != 0)
										state->prms[k] = min(MembReset, state->prms[k]+(int)((double)(MembReset-THminus)*((double)(time_act - t_last_in_spike))/((double)TLminus)));
								
                                int oldv=state->prms[k];
                                state->prms[k] += ev[2]*params->prms[state->prms[3 + n_in_ports + in_port] + KKPARAMS + (Ny_kernel - 1 - j)*Nx_kernel + i];
								
                                //check for overflow
                                if((state->prms[k]<oldv)&& (state->prms[k]<0) &&(params->prms[state->prms[3 + n_in_ports + in_port] + KKPARAMS + (Ny_kernel - 1 - j)*Nx_kernel + i]>0 ))
                                {
                                    state->prms[k]=THplus;
                                    //printf("Warning overflow: Set it to THplus\n");
                                }
                                //check for underflow
                                if((state->prms[k]>oldv)&& (state->prms[k]>0) &&(params->prms[state->prms[3 + n_in_ports + in_port] + KKPARAMS + (Ny_kernel - 1 - j)*Nx_kernel + i]<0 ))
                                {
                                    state->prms[k]=THminus;
                                    //printf("Warning underflow: Set it to THminus\n");
                                }

                                
                                if(state->prms[k]>=THplus){
									if(rectify == -1)
										evo[2] = -1;
									else
										evo[2] = 1;
									if(Reset_to_reminder == 1)
										state->prms[k] = max(state->prms[k]-THplus,MembReset);
									else
										state->prms[k] = MembReset;
									evo[0] = ev_post(x+Xmin,crop_xmin,crop_xmax,xshift_pre,x_subsmp,xshift_pos,&cropped_x);
									evo[1] = ev_post(y+Ymin,crop_ymin,crop_ymax,yshift_pre,y_subsmp,yshift_pos,&cropped_y);
									if(THplusInfo==1)
									   for(out_port=0;out_port<n_out_ports;out_port++)
										   if((cropped_x == 0) && (cropped_y ==0))
										   {
												time_busy=max(time_busy,write_event(max(time_busy,t_last_out_spike+Tmin),evo,onodes[out_port],n_repeat,delay_to_repeat));
												state->prms[k + 2*Nx_array*Ny_array] = max(time_act,t_last_out_spike+Tmin); // update t_last_out_spike
										   }
								}
								else if(state->prms[k]<=THminus){
									if(rectify == 1)
										evo[2] = 1;
									else
										evo[2] = -1;
									if(Reset_to_reminder == 1)
										state->prms[k] = min(state->prms[k]-THminus,MembReset);
									else
										state->prms[k] = MembReset;
									evo[0] = ev_post(x+Xmin,crop_xmin,crop_xmax,xshift_pre,x_subsmp,xshift_pos,&cropped_x);
									evo[1] = ev_post(y+Ymin,crop_ymin,crop_ymax,yshift_pre,y_subsmp,yshift_pos,&cropped_y);
									if(THminusInfo==1)
									   for(out_port=0;out_port<n_out_ports;out_port++)
											if((cropped_x == 0) && (cropped_y ==0))
											{
												time_busy=max(time_busy,write_event(max(time_busy,t_last_out_spike+Tmin),evo,onodes[out_port],n_repeat,delay_to_repeat));
												state->prms[k + 2*Nx_array*Ny_array] = max(time_act,t_last_out_spike+Tmin); // update t_last_out_spike
											}
								}
								
							}

							if (logStatesFlag){
								fprintf(logStates,"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
									time_act,in_port,k-(3 + 2*n_in_ports),state->prms[k],t_last_in_spike,t_last_out_spike,THplus,THminus,
									Reset_to_reminder,MembReset,TLplus,TLminus,Tmin,T_Refract,Nx_array,Ny_array,
									evo[0],evo[1],evo[2],x,y,
									Xmin,Ymin,crop_xmin,crop_ymin,crop_xmax,crop_ymax,xshift_pre,yshift_pre,x_subsmp,y_subsmp,xshift_pos,yshift_pos);
							}

						}

				}
				state->prms[3+in_port] = -1; // set prelim_ack to -1 for this in port
				state->prms[2] = time_busy;
			}
			else
				state->prms[3+in_port] = 1; // set prelim_ack to 1 for this in port
		}
	}

	if (logStatesFlag){
		fclose(logStates);
	}

	return 0;
}

int ev_post(int z,int crop_zmin,int crop_zmax,int zshift_pre,int z_subsmp,int zshift_pos,int *cropped_z){
	int z1, z2, z3;
	
	if((z>crop_zmax) || (z<crop_zmin)){
		*cropped_z = 1;
		return 0;
	}
	else{
		*cropped_z = 0;
		z1 = z + zshift_pre;
		z2 = z1/z_subsmp;
		z3 = z2 + zshift_pos;
		return z3;
	}
}

int module_conv_params(prmss *params, char *prm_file_name){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH], cc[32];
	int i, j, k, c, nonkernels_params=NKPARAMS, num, *tmp, *tmp_params, ok[NKPARAMS], okk=1, new_size;
	
	// Event-Driven Convolution module
	//
	// Common params (params required for any module):
	//
	// 18+2 non-kernel parameters in *params (after this, NK: kernel parameters, one set per input port):
	int total_integer_params; // n_params int = x : number of int params in module_conv
	int total_float_params; // n_params double = 0 : number of double params in module_conv
	int n_in_ports;// n_in_ports: number of input ports
	int n_out_ports; // n_out_ports: number of output ports
	int delay_to_process; // delay_to_process: time to process an input event: will set time_busy as well as t_pre_rqst of output events
	int delay_to_ack; // delay_to_ack: time to acknowledge an input request
	int fifo_depth; // fifo_depth: number of input events that can be accummulated and processed: time_busy - time_act <= fifo_depth * delay_to_process. If this is not met, then ack should acknowledged late enough to guarantee fifo has space for a next event
	int n_repeat; // n_repeat: number of times each input event is repeated at an output port
	int delay_to_repeat; // delay_to_repeat: delay of repeating output events
	//
	// module_conv specific params:
	//
	int Nx_array; // Nx_array: x-size of 2D integrator array
	int Ny_array; // Ny_array: y-size of 2D integrator array
	int Xmin; // Xmin: min value for x coordinate in feature map
	int Ymin; // Ymin: min value for y coordinate in feature map 
	int THplus; // positive threshold level for each pixel
	int THplusInfo; // if 1 positive output events will generated. Otherwise not.
	int THminus; // negative threshold level for each pixel
	int THminusInfo; // if 1 negative output events will generated. Otherwise not.
	int Reset_to_reminder; // if 1, membrane is reset to the excess value above/below threshol
	int MembReset; // Membrane value at Reset if Reset_to_reminder=0
	int TLplus; // positive leakage time from THplus if no input events are received. If TLplus=0, no leakage is applied
	int TLminus; // negative leakage time from THminus if no input events are received. If TLminus=0, no leakage is applied
	int Tmin; // minimum output events time separation. Defines pixel maximum firing frequency
	int T_Refract; // refractory time. After a spike, the pixel remains inactive for T_Refract
	
	// kernels: each input has its own kernel. For each input the following kernel parameters must be defined: (KKPARAMS=4)
	int Nx_kernel; // x-size of 2D kernel
	int Ny_kernel; // y-size of 2D kernel
	int Dx; // x displacement of kernel low-left vertice with respect to input event
	int Dy; // y displacement of kernel low-left vertice with respect to input event
	// After these 4 params, all (Nx_kernel x Ny_kernel) kernel values are given
	
	// output ports parameters: for each output port we define 10 parameters: (NOPARAMS=11)
	int crop_xmin; // cropped xmin at output
	int crop_xmax; // cropped xmax at output
	int crop_ymin; // cropped ymin at output
	int crop_ymax; // cropped ymax at output
	int xshift_pre; // x shift before subsampling
	int yshift_pre; // y shift before subsampling
	int x_subsmp; // subsampling x factor
	int y_subsmp; // subsampling y factor
	int xshift_pos; // x shift after subsampling
	int yshift_pos; // y shift after subsampling
	int rectify; // if 0 do not rectify, if 1 set sign=1, if -1 set sign=-1


	for(i=0;i<nonkernels_params;i++)
		ok[i]=0;
	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_conv_params\n\n",prm_file_name);
		exit(0);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of parameter file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(0);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in parameter file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(0);
	}
	
	new_size = nonkernels_params+2;
	tmp_params = (int *) malloc((new_size) * sizeof(int));
	if(tmp_params == NULL){
		printf("Error reallocating memory for tmp_params in module_conv_params\n\n");
		exit(0);
	}
	tmp_params[1] = 0; // n_double_params 0
	
	for(i=0;i<nonkernels_params;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer nonkernel params in file %s. It must be name_of_param followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(0);
		}
	
		if(strcmp(s,"n_in_ports") == 0){
			check_if_ok(&(ok[2-2]), "n_in_ports", prm_file_name);
			tmp_params[2] = num; n_in_ports=num;}
		if(strcmp(s,"n_out_ports") == 0){
			check_if_ok(&(ok[3-2]), "n_out_ports", prm_file_name);
			tmp_params[3] = num; n_out_ports=num;}
		if(strcmp(s,"delay_to_process") == 0){
			check_if_ok(&(ok[4-2]), "delay_to_process", prm_file_name);
			tmp_params[4] = num; delay_to_process=num;}
		if(strcmp(s,"delay_to_ack") == 0){
			check_if_ok(&(ok[5-2]), "delay_to_ack", prm_file_name);
			tmp_params[5] = num; delay_to_ack=num;}
		if(strcmp(s,"fifo_depth") == 0){
			check_if_ok(&(ok[6-2]), "fifo_depth", prm_file_name);
			tmp_params[6] = num; fifo_depth=num;}
		if(strcmp(s,"n_repeat") == 0){
			check_if_ok(&(ok[7-2]), "n_repeat", prm_file_name);
			tmp_params[7] = num; n_repeat=num;}
		if(strcmp(s,"delay_to_repeat") == 0){
			check_if_ok(&(ok[8-2]), "delay_to_repeat", prm_file_name);
			tmp_params[8] = num; delay_to_repeat=num;}
		if(strcmp(s,"Nx_array") == 0){
			check_if_ok(&(ok[9-2]), "Nx_array", prm_file_name);
			tmp_params[9] = num; Nx_array=num;}
		if(strcmp(s,"Ny_array") == 0){
			check_if_ok(&(ok[10-2]), "Ny_array", prm_file_name);
			tmp_params[10] = num; Ny_array=num;}
		if(strcmp(s,"THplus") == 0){
			check_if_ok(&(ok[11-2]), "THplus", prm_file_name);
			tmp_params[11] = num; THplus=num;}
		if(strcmp(s,"THplusInfo") == 0){
			check_if_ok(&(ok[12-2]), "THplusInfo", prm_file_name);
			tmp_params[12] = num; THplusInfo=num;}
		if(strcmp(s,"THminus") == 0){
			check_if_ok(&(ok[13-2]), "THminus", prm_file_name);
			tmp_params[13] = num; THminus=num;}
		if(strcmp(s,"THminusInfo") == 0){
			check_if_ok(&(ok[14-2]), "THminusInfo", prm_file_name);
			tmp_params[14] = num; THminusInfo=num;}
		if(strcmp(s,"Reset_to_reminder") == 0){
			check_if_ok(&(ok[15-2]), "Reset_to_reminder", prm_file_name);
			tmp_params[15] = num; Reset_to_reminder=num;}
		if(strcmp(s,"MembReset") == 0){
			check_if_ok(&(ok[16-2]), "MembReset", prm_file_name);
			tmp_params[16] = num; MembReset=num;}
		if(strcmp(s,"TLplus") == 0){
			check_if_ok(&(ok[17-2]), "TLplus", prm_file_name);
			tmp_params[17] = num; TLplus=num;}
		if(strcmp(s,"TLminus") == 0){
			check_if_ok(&(ok[18-2]), "TLminus", prm_file_name);
			tmp_params[18] = num; TLminus=num;}
		if(strcmp(s,"Tmin") == 0){
			check_if_ok(&(ok[19-2]), "Tmin", prm_file_name);
			tmp_params[19] = num; Tmin=num;}
		if(strcmp(s,"T_Refract") == 0){
			check_if_ok(&(ok[20-2]), "T_Refract", prm_file_name);
			tmp_params[20] = num; T_Refract=num;}
		if(strcmp(s,"Xmin") == 0){
			check_if_ok(&(ok[21-2]), "Xmin", prm_file_name);
			tmp_params[21] = num; Xmin=num;}
		if(strcmp(s,"Ymin") == 0){
			check_if_ok(&(ok[22-2]), "Ymin", prm_file_name);
			tmp_params[22] = num; Ymin=num;}
		
	}
	
	for(i=0;i<nonkernels_params;i++)
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading parameters from %s: some parameter is missing or file structure is wrong\n\n",prm_file_name);
		exit(0);
	}
	
	total_integer_params = nonkernels_params + 2;
	
	new_size = new_size + n_in_ports*KKPARAMS + n_out_ports*NOPARAMS;
	tmp = (int *) realloc(tmp_params, (new_size)*sizeof(int));
	if(tmp == NULL){
		printf("Error reallocating memory for tmp in module_conv_params\n\n");
		exit(0);
	}
	tmp_params = tmp;
	
	for(i=0;i<n_in_ports;i++){
		if(fscanf(fp,"Nx_kernel %d\n",&Nx_kernel) != 1){
			printf("Error reading Nx_kernel in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = Nx_kernel;
		
		if(fscanf(fp,"Ny_kernel %d\n",&Ny_kernel) != 1){
			printf("Error reading Ny_kernel in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = Ny_kernel;
		
		if(fscanf(fp,"Dx %d\n",&Dx) != 1){
			printf("Error reading Dx in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = Dx;
			
		if(fscanf(fp,"Dy %d\n",&Dy) != 1){
			printf("Error reading Dy in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = Dy;

		new_size = new_size + (Nx_kernel*Ny_kernel);
		tmp = realloc(tmp_params, new_size * sizeof(int));
		if(tmp == NULL){
			printf("Error reallocating memory for tmp in module_conv_params (2)\n\n");
			exit(0);
		}
		tmp_params = tmp;

		for(j=0;j<Nx_kernel*Ny_kernel;j++){
			k = 0;
			while(((c = fgetc(fp)) == ' ') || (c == '\n'));
			cc[k++] = c;
			while(((c = fgetc(fp)) != ' ') && (c != '\n'))
				cc[k++] = c;
			cc[k] = '\0';
			tmp_params[total_integer_params++] = atoi(cc);

		}
	
	}
	
	if(c != '\n')
		while((c = fgetc(fp)) != '\n');
	
	for(i=0;i<n_out_ports;i++){
		if(fscanf(fp,"crop_xmin %d\n",&crop_xmin) != 1){
			printf("Error reading crop_xmin in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = crop_xmin;
		
		if(fscanf(fp,"crop_xmax %d\n",&crop_xmax) != 1){
			printf("Error reading crop_xmax in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = crop_xmax;
		
		if(fscanf(fp,"crop_ymin %d\n",&crop_ymin) != 1){
			printf("Error reading crop_ymin in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = crop_ymin;
		
		if(fscanf(fp,"crop_ymax %d\n",&crop_ymax) != 1){
			printf("Error reading crop_ymax in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = crop_ymax;
		
		if(fscanf(fp,"xshift_pre %d\n",&xshift_pre) != 1){
			printf("Error reading xshift_pre in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = xshift_pre;
		
		if(fscanf(fp,"yshift_pre %d\n",&yshift_pre) != 1){
			printf("Error reading yshift_pre in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = yshift_pre;
		
		if(fscanf(fp,"x_subsmp %d\n",&x_subsmp) != 1){
			printf("Error reading x_subsmp in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = x_subsmp;
			
		if(fscanf(fp,"y_subsmp %d\n",&y_subsmp) != 1){
			printf("Error reading y_subsmp in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = y_subsmp;
		
		if(fscanf(fp,"xshift_pos %d\n",&xshift_pos) != 1){
			printf("Error reading xshift_pos in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = xshift_pos;
		
		if(fscanf(fp,"yshift_pos %d\n",&yshift_pos) != 1){
			printf("Error reading yshift_pos in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = yshift_pos;
		
		if(fscanf(fp,"rectify %d\n",&rectify) != 1){
			printf("Error reading rectify in file %s.\n\n",prm_file_name);
			exit(0);}
		tmp_params[total_integer_params++] = rectify;
		
		//Check if there is a logState parameter
		if(fscanf(fp,"logStates %s\n",logStatesName) == 1)
			{
				printf("Logging the internal states of module %s in %s\n",prm_file_name,logStatesName);
				logStatesFlag = 1;
			}
		else{
				logStatesFlag = 0;
			}
	}
	
	tmp_params[0] = total_integer_params; // n_int_params
	(*params).prms = tmp_params;
	
	fclose(fp);
	
	return 0;
}

int module_conv_states(prmss *states, char *prm_file_name, prmss *params){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH];
	int i, nonkernels_params=NKPARAMS, num, *tmp_states, ok[1]={0}, okk=1, offset;
	int n_in_ports, Nx_array, Ny_array, Nx, Ny, TLplus, TLminus, Tmin, T_Refract;
	
	// int states of module_conv are 3 + number of input ports (each in port has a prelim_ack state): total int state variables, total float state variables, time_busy
	// (*states).prms[0] = 2+1+(params[i].prms)[0+2]; //total number of int state variables in module_conv
	// (*states).prms[1] = 0; //total number of double state variables in module_conv
	// (*states).prms[2] = 0;  // initial time_busy
	// (*states.prms)[3+j] = -1; //flag to indicate event on input port j has been prelim-acked (-1) or is pending (1) because module was busy and fifo full
	
	n_in_ports = (*params).prms[2];
	Nx_array = (*params).prms[9];
	Ny_array = (*params).prms[10];
	TLplus = (*params).prms[17];
	TLminus= (*params).prms[18];
	Tmin = (*params).prms[19];
	T_Refract = (*params).prms[20];
	
	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_conv_params\n\n",prm_file_name);
		exit(0);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of states file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(0);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in states file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(0);
	}
	
	num = 3 + 2*n_in_ports + 3*(Nx_array * Ny_array); // total number of int state variables in module_conv
	tmp_states = (int *) malloc(num * sizeof(int));
	tmp_states[0] = num; 
	tmp_states[1] = 0; // total number of double state variables in module_conv
	
	for(i=0;i<1;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer initial state in file %s. It must be name_of_state followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(0);
		}
	
		if(strcmp(s,"time_busy_initial") == 0){
			check_if_ok(&(ok[0]), "time_busy_initial", prm_file_name);
			tmp_states[2] = num;
		}
	}
	
	for(i=0;i<1;i++) 
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading initial state from %s: some state is missing or file structure is wrong\n\n",prm_file_name);
		exit(0);
	}
	
	// initialize prelim_Ack state to -1 for all input ports
	for(i=0;i<n_in_ports;i++)
		tmp_states[i+3] = -1;
		
	// initialize pointers to kernel params
	num = nonkernels_params+2;
	offset = 3 + n_in_ports;
	for(i=offset;i<offset+n_in_ports;i++){
		tmp_states[i] = num;
		Nx = (*params).prms[num];
		Ny = (*params).prms[num+1];
		num += KKPARAMS + Nx * Ny;
	}
		
	// initialize to 0 membranes states
	offset = 3 + 2*n_in_ports;
	for(i=offset;i<offset+Nx_array*Ny_array;i++)
		tmp_states[i] = 0;
		
	// initialize t_last_in_spike and t_last_out_spike
	offset += Nx_array*Ny_array;
	for(i=offset;i<offset+2*Nx_array*Ny_array;i++)
			tmp_states[i] = -max(max(T_Refract+1,Tmin+1),max(TLplus+1,TLminus+1));
			
		
	(*states).prms = tmp_states;
	
	fclose(fp);
	
	return 0;
}


